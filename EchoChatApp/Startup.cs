using EchoChat.Channel;
using EchoChat.ChannelConnector;
using EchoChat.DB;
using EchoChat.Repository;
using EchoChat.ServiceProvider;
using EchoChatApp.CronJobServices;
using EchoChatApp.CronJobServices.CronJobExtensionMethods;
using EchoChatApp.Helper;
using EchoChatApp.Hubs;
using EchoChatApp.Middleware;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System;
using System.Configuration;
using System.IO;
using System.Text;

namespace EchoChatApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCronJob<ScheduleMessageSynced>(c =>
            {
                c.TimeZoneInfo = TimeZoneInfo.Local;
                c.CronExpression = @"* * * * *"; // Run at every minutes
                //c.CronExpression = @"*/4 * * * *"; // Run every 4th minutes
                //c.CronExpression = @"0 0 1 * *"; // Run at midnight, on day 1 of every month
                //c.CronExpression = @"0 10 * * SUN"; // Run at 10:00 on Sunday.
                //c.CronExpression = @"0 1 30 * * "; // Run at 01:00 on day-of-month 30.
                //c.CronExpression = @"0 2 * * *"; // Run at every day at 2 AM
            });

            services.AddCronJob<ScheduleChannelPredictionFileSynced>(c =>
            {
                c.TimeZoneInfo = TimeZoneInfo.Local;
                //c.CronExpression = @"* * * * *"; // Run at every minutes
                c.CronExpression = @"0 */6 * * *"; // Run at every 6 hours
            });

            if (Configuration.GetConnectionString("DBConnection") != null)
            {
                services.AddDbContext<EchoDBContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DBConnection"),
                    sqlServerOptions => sqlServerOptions.CommandTimeout(300)).EnableSensitiveDataLogging());
            }

            services.AddHttpClient();
            services.AddAutoMapper(typeof(Startup));
            services.AddControllers();
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "EchoChatApp", Version = "v1" });

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer",
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Description = "JWT Authorization header using the Bearer scheme."
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                          new OpenApiSecurityScheme
                            {
                                Reference = new OpenApiReference
                                {
                                    Type = ReferenceType.SecurityScheme,
                                    Id = "Bearer"
                                }
                            },
                            new string[] {}

                    }
                });
            });

            // configure strongly typed settings objects
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettingsFromFile>(appSettingsSection);

            // configure jwt authentication
            var appSettings = appSettingsSection.Get<AppSettingsFromFile>();

            services.AddCors(c =>
            {
                c.AddPolicy("AllowOrigin", options => options.AllowAnyOrigin());
            });

            services.AddSignalR();
            services.AddSignalR(hubOptions =>
            {
                hubOptions.MaximumReceiveMessageSize = 10240;  // bytes
                hubOptions.KeepAliveInterval = TimeSpan.FromMinutes(3);
                hubOptions.ClientTimeoutInterval = TimeSpan.FromMinutes(6);
                hubOptions.EnableDetailedErrors = true;
            });

            services.AddControllers()
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.PropertyNamingPolicy = null;
                });

            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

            // If using IIS:
            services.Configure<IISServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });

            services.AddScoped<ChatHub, ChatHub>();
            services.AddScoped<SPToCoreContext>();
            services.AddScoped<IRepositoryBase, RepositoryBase>();
            services.AddScoped<IAuthRepository, AuthRepository>();
            services.AddScoped<INoticeRepository, NoticeRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IWebsiteRepository, WebsiteRepository>();
            services.AddScoped<IClientRepository, ClientRepository>();
            services.AddScoped<IReportRepository, ReportRepository>();
            services.AddScoped<IAppRepository, AppRepository>();
            services.AddScoped<IGroupRepository, GroupRepository>();
            services.AddScoped<ICommonRepository, CommonRepository>();
            services.AddScoped<IHookRepository, HookRepository>();
            services.AddScoped<ISchedularRepository, SchedularRepository>();
            services.AddScoped<ITemplateRepository, TemplateRepository>();

            services.AddScoped<IAuthProvider, AuthProvider>();
            services.AddScoped<INoticeProvider, NoticeProvider>();
            services.AddScoped<IUserProvider, UserProvider>();
            services.AddScoped<IWebsiteProvider, WebsiteProvider>();
            services.AddScoped<IClientProvider, ClientProvider>();
            services.AddScoped<IReportProvider, ReportProvider>();
            services.AddScoped<IAppProvider, AppProvider>();
            services.AddScoped<IGroupProvider, GroupProvider>();
            services.AddScoped<ICommonProvider, CommonProvider>();
            services.AddScoped<IHookProvider, HookProvider>();
            services.AddScoped<ISchedularProvider, SchedularProvider>();
            services.AddScoped<ITemplateProvider, TemplateProvider>();

            services.AddScoped<IConnector, Connector>();
            services.AddScoped<IBroadcaster, Broadcaster>();

            services.AddScoped<IAppSetting, EchoChatApp.Helper.AppSetting>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddLog4Net();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "EchoChatApp v1"));
            }

            app.UseCors(builder => builder
               .AllowAnyHeader()
               .AllowAnyMethod()
               .SetIsOriginAllowed(_ => true)
               .AllowCredentials()
            );

            app.Use(async (context, next) =>
            {
                context.Response.Headers.Add("X-Xss-Protection", "1");
                await next();
            });

            app.UseHttpsRedirection();
            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<ChatHub>("/chathub");
            });

            app.UseMiddleware<ExceptionHandler>();
        }
    }
}
