﻿using EchoChat.Helper;
using EchoChat.Models;
using EchoChat.ServiceProvider;
using EchoChatApp.General;
using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace EchoChatApp.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class GroupController : BaseController
    {
        private IGroupProvider groupProvider;
        private ILog logger;
        private IUserProvider userProvider;
        private INoticeProvider noticeProvider;
        public GroupController(IGroupProvider oGroupProvider, IUserProvider oUserProvider, INoticeProvider oNoticeProvider)
        {
            groupProvider = oGroupProvider;
            userProvider = oUserProvider;
            noticeProvider = oNoticeProvider;
            logger = Logger.GetLogger(typeof(GroupController));
        }

        [HttpGet("GetUserGroups")]
        public IActionResult GetUserGroups(string freeText, int pageSize = 25, int PageNo = 1)
        {
            try
            {
                UserGroupModel result = groupProvider.GetUserGroups(UserID, freeText, pageSize, PageNo, TimeZoneOffset);
                return Ok(new { result });
            }
            catch (Exception ex)
            {
                logger.Error("GetUserGroups: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [Route("CreateUpdateGroup")]
        [HttpPost]
        public IActionResult CreateUpdateGroup(GroupDTO group)
        {
            try
            {
                if (string.IsNullOrEmpty(group.Name) || string.IsNullOrEmpty(group.Name.Trim()))
                {
                    return BadRequest(new { Message = "Please enter group name" });
                }

                bool isExit = groupProvider.CheckGroupAlreadyExists(group.GroupID, group.Name);
                if (isExit)
                {
                    return BadRequest(new { Message = "We're sorry, Group with same name is already exists" });
                }
                group.ClientID = ClientID;
                group.CreatedBy = UserID;
                group.IsPrivate = Guid.TryParse(group.Name, out Guid guidOutput);
                GroupDTO result = groupProvider.CreateUpdateGroup(group);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error("CreateGroup: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpGet("GetMemberUploadFile")]
        public async Task<IActionResult> GetMemberUploadFile()
        {
            try
            {
                byte[] myfile = await new WebClient().DownloadDataTaskAsync(new Uri(AWSHelper.GetPresignedFileURL("MemberUploadFormat.xlsx")));
                return File(myfile, "image/png", "MemberUploadFormat.xlsx");
            }
            catch (Exception ex)
            {
                logger.Error("GetMemberUploadFile: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpPost]
        [Route("DeleteGroup")]
        public IActionResult DeleteGroup(int groupID)
        {
            try
            {
                groupProvider.DeleteGroup(groupID);
                return Ok(new { DeleteGroupResult = "1" });
            }
            catch (Exception ex)
            {
                logger.Error("DeleteGroup: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [Route("GetMembersByGroupID")]
        [HttpGet]
        public IActionResult GetMembersByGroupID(int groupID, int pageSize, int PageNo, string freeText)
        {
            try
            {
                ListOrGroupWiseUsers groupWiseUsers = groupProvider.GetMembersByGroupID(groupID, pageSize, PageNo, freeText, TimeZoneOffset);
                return Ok(groupWiseUsers);
            }
            catch (Exception ex)
            {
                logger.Error("GetMembersByGroupID: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [Route("CreateUpdateList")]
        [HttpPost]
        public IActionResult CreateUpdateList(ListDTO list)
        {
            try
            {
                if (string.IsNullOrEmpty(list.Name) || string.IsNullOrEmpty(list.Name.Trim()))
                {
                    return BadRequest(new { Message = "Please enter list name" });
                }

                bool isExit = groupProvider.CheckListAlreadyExists(list.ListID, list.Name);
                if (isExit)
                {
                    return BadRequest(new { Message = "We're sorry, List  with same name is already exists" });
                }
                list.ClientID = ClientID;
                list.CreatedBy = UserID;
                groupProvider.CreateUpdateList(list);
                return Ok();
            }
            catch (Exception ex)
            {
                logger.Error("CreateUpdateList: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [Route("DeleteList")]
        [HttpPost]
        public IActionResult DeleteList(int listID)
        {
            try
            {
                groupProvider.DeleteList(listID);
                return Ok();
            }
            catch (Exception ex)
            {
                logger.Error("DeleteList: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [Route("SetGroupListMapping")]
        [HttpPost]
        public IActionResult SetGroupListMapping(GroupListMapping mapping)
        {
            try
            {
                groupProvider.SetGroupListMapping(mapping);
                return Ok();
            }
            catch (Exception ex)
            {
                logger.Error("SetGroupListMapping: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [Route("GetAllListByClientID")]
        [HttpGet]
        public IActionResult GetAllListByClientID(string freeText)
        {
            try
            {
                List<ListDTO> result = groupProvider.GetAllListByClientID(ClientID, freeText, TimeZoneOffset);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error("GetAllListByClientID: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [Route("GetAllGroupsByClientID")]
        [HttpGet]
        public IActionResult GetAllGroupsByClientID()
        {
            try
            {
                List<GroupDTO> result = groupProvider.GetAllGroupsByClientID(ClientID);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error("GetAllGroupsByClientID: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [Route("GetAllListByGroupID")]
        [HttpGet]
        public IActionResult GetAllListByGroupID(int groupID)
        {
            try
            {
                List<ListSmallDTO> result = groupProvider.GetAllListByGroupID(groupID);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error("GetAllListByGroupID: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [Route("GetMembersByList")]
        [HttpGet]
        public IActionResult GetMembersByList(int listID, int pageSize, int PageNo, string freeText)
        {
            try
            {
                ListOrGroupWiseUsers groupWiseUsers = groupProvider.GetMembersByListID(listID, pageSize, PageNo, freeText, TimeZoneOffset);
                return Ok(groupWiseUsers);
            }
            catch (Exception ex)
            {
                logger.Error("GetMembersByList: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [Route("CreatePrivateGroup")]
        [HttpPost]
        public IActionResult CreatePrivateGroup(Guid userID, int noticeID)
        {
            try
            {
                UserProfile uModel = userProvider.GetUserByID(userID);
                int groupID = groupProvider.CheckPrivateGroupAlreadyExists(uModel.FirstName, uModel.MobileNo);
                if (groupID == 0)
                {
                    GroupDTO groupDTO = new GroupDTO();
                    groupDTO.Name = uModel.FirstName;
                    groupDTO.Description = uModel.MobileNo;
                    groupDTO.IsPrivate = true;
                    groupDTO.CreatedBy = UserID;
                    groupDTO.ClientID = ClientID;
                    List<Guid> lstGUsers = new List<Guid>();
                    lstGUsers.Add(groupDTO.CreatedBy);
                    lstGUsers.Add(userID);
                    groupProvider.CreatePrivateGroup(groupDTO, lstGUsers);
                    groupID = groupDTO.GroupID;
                }
                NoticeDetailDTO nd = noticeProvider.GetNoticeDetailsByNoticeID(noticeID);
                if (nd != null)
                {
                    NoticeDetailDTO _notice = new NoticeDetailDTO();
                    _notice.NoticeTitle = nd.NoticeTitle;
                    _notice.NoticeDetail = nd.NoticeDetail;
                    _notice.NoticeDetailHTML = nd.NoticeDetailHTML;
                    _notice.GroupID = groupID;
                    _notice.CreatedBy = UserID;
                    _notice.IsReply = true;
                    _notice.IsSms = nd.IsSms;
                    _notice.ParentID = 0;
                    _notice.FileType = 0;
                    _notice.FileName = null;
                    _notice.IsEmail = nd.IsEmail;
                    _notice.IsWhatsapp = nd.IsWhatsapp;
                    _notice.IsFacebook = false;

                    noticeProvider.SavePrivateNoticeDetails(_notice);

                    List<UserChannelInfo> userChannelInfos = new List<UserChannelInfo>();
                    UserChannelDataWithGroup mapping = groupProvider.GetGroupMappingUsers(groupID.ToString(), _notice.ParentID ?? 0, _notice.NoticeDetail);
                    userChannelInfos.AddRange(mapping.UserChannelInfoList);

                    noticeProvider.SavePrivateNotice(userChannelInfos, _notice.NoticeID, groupID, UserID);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                logger.Error("AnalyzeNoticeDetails: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [Route("CheckDNDStatusByList")]
        [HttpPost]
        public IActionResult CheckDNDStatusByList(int listID)
        {
            try
            {
                List<DNDUserModel> listWiseUsers = groupProvider.GetAllMembersByListID(listID);
                List<DNDStatusModel> listDNDStatus = new List<DNDStatusModel>();
                foreach (DNDUserModel user in listWiseUsers)
                {
                    DNDStatusModel dNDStatusModel = new DNDStatusModel();
                    string waAPI = string.Format("http://manthanitsolutions.com/dnd/api/dnd_check_restapi.php?api_key=hJcMuPFmdW3Oj1HR&pass=u9gDRGBnYf&mobile=91{0}", user.MobileNo);
                    var client = new RestClient(waAPI);
                    client.Timeout = -1;
                    var request = new RestRequest(Method.GET);
                    IRestResponse response = client.Execute(request);
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        dNDStatusModel.Status = response.Content.Split(':')[1];
                        dNDStatusModel.UserID = user.UserID;
                    }
                    listDNDStatus.Add(dNDStatusModel);
                }

                bool result =  groupProvider.SaveDNDStatus(UserID, listDNDStatus);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Error("CheckDNDStatusByList: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [Route("GetDNDReoprt")]
        [HttpGet]
        public IActionResult GetDNDReoprt(int pageSize, int pageNo,string freeText)
        {
            try
            {
                DNDReportDetailsModel dNDReport = groupProvider.GetDNDReoprt(ClientID, pageSize, pageNo, freeText, TimeZoneOffset);
                return Ok(dNDReport);
            }
            catch (Exception ex)
            {
                logger.Error("GetMembersByList: " + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }
    }
}