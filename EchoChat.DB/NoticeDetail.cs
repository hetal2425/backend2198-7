﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EchoChat.DB
{
    public partial class NoticeDetails
    {
        public int NoticeId { get; set; }
        public string NoticeTitle { get; set; }
        public string NoticeDetail { get; set; }
        public DateTime NoticeDate { get; set; }
        public int? GroupId { get; set; }
        public int? ParentId { get; set; }
        public Guid? CreatedBy { get; set; }
        public string FileName { get; set; }
        public int? FileType { get; set; }
        public bool? IsReply { get; set; }
        public string LatestMessage { get; set; }
        public bool? IsSms { get; set; }
        public bool? IsEmail { get; set; }
        public bool? IsWhatsapp { get; set; }
        public bool? IsFacebook { get; set; }
        public bool? IsViber { get; set; }
        public bool? IsTelegram { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
    }
}
