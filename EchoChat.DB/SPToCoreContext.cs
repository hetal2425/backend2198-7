using System;
using System.Data;
using System.Collections.Generic;
using Microsoft.Data.SqlClient;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace EchoChat.DB
{
    public partial class SPToCoreContext : EchoDBContext
    {
        private DbSet<usp_GetGroupMappingUsersResult> usp_GetGroupMappingUsers { get; set; }
        private DbSet<usp_GetUsersForPendingNoticeResult> usp_GetUsersForPendingNotice { get; set; }
        private DbSet<usp_GetNoticesListResult> usp_GetNoticesList { get; set; }
        private DbSet<usp_GetNoticesList_RepliesResult> usp_GetNoticesList_Replies { get; set; }
        private DbSet<usp_GetUserGroupsResult> usp_GetUserGroups { get; set; }
        private DbSet<usp_GetUserLoginResult> usp_GetUserLogin { get; set; }
        private DbSet<usp_InserMsgWAResult> usp_InserMsgWA { get; set; }
        private DbSet<usp_User_SelectByGroupIDResult> usp_User_SelectByGroupID { get; set; }
        private DbSet<usp_User_SelectByListIDResult> usp_User_SelectByListID { get; set; }
        private DbSet<usp_InsertReceiveEmailResult> usp_InsertReceiveEmail { get; set; }
        private DbSet<usp_InsertReceiveSlackResult> usp_InsertReceiveSlack { get; set; }
        private DbSet<usp_GetScheduledNoticeListResult> usp_GetScheduledNoticeList { get; set; }
        private DbSet<usp_Report_GetCampaignReportDetailsResult> usp_Report_GetCampaignReportDetails { get; set; }
        private DbSet<usp_Report_GetCampaignReportHeadingResult> usp_Report_GetCampaignReportHeading { get; set; }
        private DbSet<usp_Report_GetGroupReportDetailsResult> usp_Report_GetGroupReportDetails { get; set; }
        private DbSet<usp_Report_GetGroupReportHeadingResult> usp_Report_GetGroupReportHeading { get; set; }
        private DbSet<usp_Report_GetReadReportDetailsResult> usp_Report_GetReadReportDetails { get; set; }
        private DbSet<usp_GetListByClientIDResult> usp_GetListByClientID { get; set; }
        private DbSet<usp_Report_ChannelVerificationResult> usp_Report_ChannelVerification { get; set; }
        private DbSet<usp_Report_GetCampaignSummaryReportResult> usp_Report_GetCampaignSummaryReport { get; set; }
        private DbSet<usp_Report_GetChannelInsightReportResult> usp_Report_GetChannelInsightReport { get; set; }
        private DbSet<usp_Report_GetChannelSavingReportResult> usp_Report_GetChannelSavingReport { get; set; }
        private DbSet<usp_Report_GetChannelTrendsReportResult> usp_Report_GetChannelTrendsReport { get; set; }
        private DbSet<usp_Report_GetWeekdaysEngagementReportResult> usp_Report_GetWeekdaysEngagementReport { get; set; }
        private DbSet<usp_GetSchedularDataResult> usp_GetSchedularData { get; set; }
        private DbSet<usp_GetTemplateDataResult> usp_GetTemplateData { get; set; }
        private DbSet<usp_GetUserIdsFromGroupResult> usp_GetUserIdsFromGroupData { get; set; }
        private DbSet<usp_GetNoticeDetailsForHubResult> usp_GetNoticeDetailsForHubData { get; set; }
        private DbSet<usp_ChannelPredictionFileResult> usp_ChannelPredictionFileData { get; set; }
        private DbSet<usp_GetChannelPredictionResult> usp_GetChannelPredictionData { get; set; }
        private DbSet<usp_Report_GetDNDReportDetailsResult> usp_Report_GetDNDReportDetails { get; set; }
        public SPToCoreContext()
        {
        }

        public SPToCoreContext(DbContextOptions<EchoDBContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // No key
            modelBuilder.Entity<usp_GetGroupMappingUsersResult>().HasNoKey();
            modelBuilder.Entity<usp_GetNoticesListResult>().HasNoKey();
            modelBuilder.Entity<usp_GetNoticesList_RepliesResult>().HasNoKey();
            modelBuilder.Entity<usp_GetUserGroupsResult>().HasNoKey();
            modelBuilder.Entity<usp_GetUserLoginResult>().HasNoKey();
            modelBuilder.Entity<usp_InserMsgWAResult>().HasNoKey();
            modelBuilder.Entity<usp_User_SelectByGroupIDResult>().HasNoKey();
            modelBuilder.Entity<usp_User_SelectByListIDResult>().HasNoKey();
            modelBuilder.Entity<usp_GetScheduledNoticeListResult>().HasNoKey();
            modelBuilder.Entity<usp_InsertReceiveEmailResult>().HasNoKey();
            modelBuilder.Entity<usp_InsertReceiveSlackResult>().HasNoKey();
            modelBuilder.Entity<usp_Report_GetCampaignReportDetailsResult>().HasNoKey();
            modelBuilder.Entity<usp_Report_GetCampaignReportHeadingResult>().HasNoKey();
            modelBuilder.Entity<usp_Report_GetGroupReportDetailsResult>().HasNoKey();
            modelBuilder.Entity<usp_Report_GetGroupReportHeadingResult>().HasNoKey();
            modelBuilder.Entity<usp_Report_GetReadReportDetailsResult>().HasNoKey();
            modelBuilder.Entity<usp_GetListByClientIDResult>().HasNoKey();
            modelBuilder.Entity<usp_Report_ChannelVerificationResult>().HasNoKey();
            modelBuilder.Entity<usp_Report_GetCampaignSummaryReportResult>().HasNoKey();
            modelBuilder.Entity<usp_Report_GetChannelInsightReportResult>().HasNoKey();
            modelBuilder.Entity<usp_Report_GetChannelSavingReportResult>().HasNoKey();
            modelBuilder.Entity<usp_Report_GetChannelTrendsReportResult>().HasNoKey();
            modelBuilder.Entity<usp_GetUsersForPendingNoticeResult>().HasNoKey();
            modelBuilder.Entity<usp_Report_GetWeekdaysEngagementReportResult>().HasNoKey();
            modelBuilder.Entity<usp_GetUserIdsFromGroupResult>().HasNoKey();
            modelBuilder.Entity<usp_GetNoticeDetailsForHubResult>().HasNoKey();
            modelBuilder.Entity<usp_ChannelPredictionFileResult>().HasNoKey();
            modelBuilder.Entity<usp_GetChannelPredictionResult>().HasNoKey();
            modelBuilder.Entity<usp_Report_GetDNDReportDetailsResult>().HasNoKey();
            base.OnModelCreating(modelBuilder);
        }

        public async Task<List<usp_GetUsersForPendingNoticeResult>> usp_GetUsersForPendingNoticeAsync(int? NoticeID, bool? IsSMS, bool? IsEmail, bool? IsWA)
        {
            //Initialize Result 
            List<usp_GetUsersForPendingNoticeResult> lst = new List<usp_GetUsersForPendingNoticeResult>();
            try
            {
                // Parameters
                SqlParameter p_NoticeID = new SqlParameter("@NoticeID", NoticeID ?? (object)DBNull.Value);
                p_NoticeID.Direction = ParameterDirection.Input;
                p_NoticeID.DbType = DbType.Int32;
                p_NoticeID.Size = 4;

                SqlParameter p_IsSMS = new SqlParameter("@IsSMS", IsSMS ?? (object)DBNull.Value);
                p_IsSMS.Direction = ParameterDirection.Input;
                p_IsSMS.DbType = DbType.Boolean;
                p_IsSMS.Size = 1;

                SqlParameter p_IsEmail = new SqlParameter("@IsEmail", IsEmail ?? (object)DBNull.Value);
                p_IsEmail.Direction = ParameterDirection.Input;
                p_IsEmail.DbType = DbType.Boolean;
                p_IsEmail.Size = 1;

                SqlParameter p_IsWA = new SqlParameter("@IsWA", IsWA ?? (object)DBNull.Value);
                p_IsWA.Direction = ParameterDirection.Input;
                p_IsWA.DbType = DbType.Boolean;
                p_IsWA.Size = 1;


                // Processing 
                string sqlQuery = $@"EXEC [dbo].[usp_GetUsersForPendingNotice] @NoticeID, @IsSMS, @IsEmail, @IsWA";

                //Output Data
                lst = await this.usp_GetUsersForPendingNotice.FromSqlRaw(sqlQuery, p_NoticeID, p_IsSMS, p_IsEmail, p_IsWA).ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
            return lst;
        }

        public async Task<List<usp_Report_GetChannelTrendsReportResult>> usp_Report_GetChannelProgressReportAsync(int? ClientID, int? TimeZoneOffset)
        {
            //Initialize Result 
            List<usp_Report_GetChannelTrendsReportResult> lst = new List<usp_Report_GetChannelTrendsReportResult>();
            try
            {
                // Parameters
                SqlParameter p_ClientID = new SqlParameter("@ClientID", ClientID ?? (object)DBNull.Value);
                p_ClientID.Direction = ParameterDirection.Input;
                p_ClientID.DbType = DbType.Int32;
                p_ClientID.Size = 4;

                SqlParameter p_TimeZoneOffset = new SqlParameter("@TimeZoneOffset", TimeZoneOffset ?? (object)DBNull.Value);
                p_TimeZoneOffset.Direction = ParameterDirection.Input;
                p_TimeZoneOffset.DbType = DbType.Int32;
                p_TimeZoneOffset.Size = 4;

                // Processing 
                string sqlQuery = $@"EXEC [dbo].[usp_Report_GetChannelTrendsReport] @ClientID, @TimeZoneOffset";

                //Output Data
                lst = await this.usp_Report_GetChannelTrendsReport.FromSqlRaw(sqlQuery, p_ClientID, p_TimeZoneOffset).ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
            return lst;
        }

        public async Task<List<usp_Report_GetCampaignSummaryReportResult>> usp_Report_GetCampaignSummaryReportAsync(int? ClientID, int? GroupID, int? NoticeID, DateTime? FromDate, DateTime? ToDate, int? TimeZoneOffset)
        {
            //Initialize Result 
            List<usp_Report_GetCampaignSummaryReportResult> lst = new List<usp_Report_GetCampaignSummaryReportResult>();
            try
            {
                // Parameters
                SqlParameter p_ClientID = new SqlParameter("@ClientID", ClientID ?? (object)DBNull.Value);
                p_ClientID.Direction = ParameterDirection.Input;
                p_ClientID.DbType = DbType.Int32;
                p_ClientID.Size = 4;

                SqlParameter p_GroupID = new SqlParameter("@GroupID", GroupID ?? (object)DBNull.Value);
                p_GroupID.Direction = ParameterDirection.Input;
                p_GroupID.DbType = DbType.Int32;
                p_GroupID.Size = 4;

                SqlParameter p_NoticeID = new SqlParameter("@NoticeID", NoticeID ?? (object)DBNull.Value);
                p_NoticeID.Direction = ParameterDirection.Input;
                p_NoticeID.DbType = DbType.Int32;
                p_NoticeID.Size = 4;

                SqlParameter p_FromDate = new SqlParameter("@FromDate", FromDate ?? (object)DBNull.Value);
                p_FromDate.Direction = ParameterDirection.Input;
                p_FromDate.DbType = DbType.DateTime;
                p_FromDate.Size = 8;

                SqlParameter p_ToDate = new SqlParameter("@ToDate", ToDate ?? (object)DBNull.Value);
                p_ToDate.Direction = ParameterDirection.Input;
                p_ToDate.DbType = DbType.DateTime;
                p_ToDate.Size = 8;

                SqlParameter p_TimeZoneOffset = new SqlParameter("@TimeZoneOffset", TimeZoneOffset ?? (object)DBNull.Value);
                p_TimeZoneOffset.Direction = ParameterDirection.Input;
                p_TimeZoneOffset.DbType = DbType.Int32;
                p_TimeZoneOffset.Size = 4;

                // Processing 
                string sqlQuery = $@"EXEC [dbo].[usp_Report_GetCampaignSummaryReport] @ClientID, @GroupID, @NoticeID, @FromDate, @ToDate, @TimeZoneOffset";

                //Output Data
                lst = await this.usp_Report_GetCampaignSummaryReport.FromSqlRaw(sqlQuery, p_ClientID, p_GroupID, p_NoticeID, p_FromDate, p_ToDate, p_TimeZoneOffset).ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
            return lst;
        }

        public async Task<List<usp_Report_GetChannelInsightReportResult>> usp_Report_GetChannelInsightReportAsync(int? ClientID, int? GroupID, int? NoticeID, DateTime? FromDate, DateTime? ToDate, int? TimeSlot, int? TimeZoneOffset)
        {
            //Initialize Result 
            List<usp_Report_GetChannelInsightReportResult> lst = new List<usp_Report_GetChannelInsightReportResult>();
            try
            {
                // Parameters
                SqlParameter p_ClientID = new SqlParameter("@ClientID", ClientID ?? (object)DBNull.Value);
                p_ClientID.Direction = ParameterDirection.Input;
                p_ClientID.DbType = DbType.Int32;
                p_ClientID.Size = 4;

                SqlParameter p_GroupID = new SqlParameter("@GroupID", GroupID ?? (object)DBNull.Value);
                p_GroupID.Direction = ParameterDirection.Input;
                p_GroupID.DbType = DbType.Int32;
                p_GroupID.Size = 4;

                SqlParameter p_NoticeID = new SqlParameter("@NoticeID", NoticeID ?? (object)DBNull.Value);
                p_NoticeID.Direction = ParameterDirection.Input;
                p_NoticeID.DbType = DbType.Int32;
                p_NoticeID.Size = 4;

                SqlParameter p_FromDate = new SqlParameter("@FromDate", FromDate ?? (object)DBNull.Value);
                p_FromDate.Direction = ParameterDirection.Input;
                p_FromDate.DbType = DbType.DateTime;
                p_FromDate.Size = 8;

                SqlParameter p_ToDate = new SqlParameter("@ToDate", ToDate ?? (object)DBNull.Value);
                p_ToDate.Direction = ParameterDirection.Input;
                p_ToDate.DbType = DbType.DateTime;
                p_ToDate.Size = 8;

                SqlParameter p_TimeSlot = new SqlParameter("@TimeSlot", TimeSlot ?? (object)DBNull.Value);
                p_TimeSlot.Direction = ParameterDirection.Input;
                p_TimeSlot.DbType = DbType.Int32;
                p_TimeSlot.Size = 4;

                SqlParameter p_TimeZoneOffset = new SqlParameter("@TimeZoneOffset", TimeZoneOffset ?? (object)DBNull.Value);
                p_TimeZoneOffset.Direction = ParameterDirection.Input;
                p_TimeZoneOffset.DbType = DbType.Int32;
                p_TimeZoneOffset.Size = 4;


                // Processing 
                string sqlQuery = $@"EXEC [dbo].[usp_Report_GetChannelInsightReport] @ClientID, @GroupID, @NoticeID, @FromDate, @ToDate, @TimeSlot, @TimeZoneOffset";

                //Output Data
                lst = await this.usp_Report_GetChannelInsightReport.FromSqlRaw(sqlQuery, p_ClientID, p_GroupID, p_NoticeID, p_FromDate, p_ToDate, p_TimeSlot, p_TimeZoneOffset).ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
            return lst;
        }

        public async Task<List<usp_Report_GetWeekdaysEngagementReportResult>> usp_Report_GetWeekdaysEngagementReportAsync(int? ClientID, DateTime? FromDate, DateTime? ToDate, int? TimeSlot, int? TimeZoneOffset)
        {
            //Initialize Result 
            List<usp_Report_GetWeekdaysEngagementReportResult> lst = new List<usp_Report_GetWeekdaysEngagementReportResult>();
            try
            {
                // Parameters
                SqlParameter p_ClientID = new SqlParameter("@ClientID", ClientID ?? (object)DBNull.Value);
                p_ClientID.Direction = ParameterDirection.Input;
                p_ClientID.DbType = DbType.Int32;
                p_ClientID.Size = 4;

                SqlParameter p_FromDate = new SqlParameter("@FromDate", FromDate ?? (object)DBNull.Value);
                p_FromDate.Direction = ParameterDirection.Input;
                p_FromDate.DbType = DbType.DateTime;
                p_FromDate.Size = 8;

                SqlParameter p_ToDate = new SqlParameter("@ToDate", ToDate ?? (object)DBNull.Value);
                p_ToDate.Direction = ParameterDirection.Input;
                p_ToDate.DbType = DbType.DateTime;
                p_ToDate.Size = 8;

                SqlParameter p_TimeSlot = new SqlParameter("@TimeSlot", TimeSlot ?? (object)DBNull.Value);
                p_TimeSlot.Direction = ParameterDirection.Input;
                p_TimeSlot.DbType = DbType.Int32;
                p_TimeSlot.Size = 4;

                SqlParameter p_TimeZoneOffset = new SqlParameter("@TimeZoneOffset", TimeZoneOffset ?? (object)DBNull.Value);
                p_TimeZoneOffset.Direction = ParameterDirection.Input;
                p_TimeZoneOffset.DbType = DbType.Int32;
                p_TimeZoneOffset.Size = 4;


                // Processing 
                string sqlQuery = $@"EXEC [dbo].[usp_Report_GetWeekdaysEngagementReport] @ClientID, @FromDate, @ToDate, @TimeSlot, @TimeZoneOffset";

                //Output Data
                lst = await this.usp_Report_GetWeekdaysEngagementReport.FromSqlRaw(sqlQuery, p_ClientID, p_FromDate, p_ToDate, p_TimeSlot, p_TimeZoneOffset).ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
            return lst;
        }

        public async Task<List<usp_Report_GetChannelSavingReportResult>> usp_Report_GetChannelSavingReportAsync(int? ClientID, DateTime? FromDate, DateTime? ToDate, int? TimeZoneOffset)
        {
            //Initialize Result 
            List<usp_Report_GetChannelSavingReportResult> lst = new List<usp_Report_GetChannelSavingReportResult>();
            try
            {
                // Parameters
                SqlParameter p_ClientID = new SqlParameter("@ClientID", ClientID ?? (object)DBNull.Value);
                p_ClientID.Direction = ParameterDirection.Input;
                p_ClientID.DbType = DbType.Int32;
                p_ClientID.Size = 4;

                SqlParameter p_FromDate = new SqlParameter("@FromDate", FromDate ?? (object)DBNull.Value);
                p_FromDate.Direction = ParameterDirection.Input;
                p_FromDate.DbType = DbType.DateTime;
                p_FromDate.Size = 8;

                SqlParameter p_ToDate = new SqlParameter("@ToDate", ToDate ?? (object)DBNull.Value);
                p_ToDate.Direction = ParameterDirection.Input;
                p_ToDate.DbType = DbType.DateTime;
                p_ToDate.Size = 8;

                SqlParameter p_TimeZoneOffset = new SqlParameter("@TimeZoneOffset", TimeZoneOffset ?? (object)DBNull.Value);
                p_TimeZoneOffset.Direction = ParameterDirection.Input;
                p_TimeZoneOffset.DbType = DbType.Int32;
                p_TimeZoneOffset.Size = 4;

                // Processing 
                string sqlQuery = $@"EXEC [dbo].[usp_Report_GetChannelSavingReport] @ClientID, @FromDate, @ToDate, @TimeZoneOffset";

                //Output Data
                lst = await this.usp_Report_GetChannelSavingReport.FromSqlRaw(sqlQuery, p_ClientID, p_FromDate, p_ToDate, p_TimeZoneOffset).ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
            return lst;
        }

        public List<usp_Report_ChannelVerificationResult> usp_Report_ChannelVerificationAsync(int? ClientID, int? PageSize, int? PageNo, string FreeText, ref int? Total)
        {
            //Initialize Result 
            List<usp_Report_ChannelVerificationResult> lst = new List<usp_Report_ChannelVerificationResult>();
            try
            {
                // Parameters
                SqlParameter p_ClientID = new SqlParameter("@ClientID", ClientID ?? (object)DBNull.Value);
                p_ClientID.Direction = ParameterDirection.Input;
                p_ClientID.DbType = DbType.Int32;
                p_ClientID.Size = 4;

                SqlParameter p_PageSize = new SqlParameter("@PageSize", PageSize ?? (object)DBNull.Value);
                p_PageSize.Direction = ParameterDirection.Input;
                p_PageSize.DbType = DbType.Int32;
                p_PageSize.Size = 4;

                SqlParameter p_PageNo = new SqlParameter("@PageNo", PageNo ?? (object)DBNull.Value);
                p_PageNo.Direction = ParameterDirection.Input;
                p_PageNo.DbType = DbType.Int32;
                p_PageNo.Size = 4;

                SqlParameter p_FreeText = new SqlParameter("@FreeText", FreeText ?? (object)DBNull.Value);
                p_FreeText.Direction = ParameterDirection.Input;
                p_FreeText.DbType = DbType.String;
                p_FreeText.Size = 100;

                SqlParameter p_Total = new SqlParameter("@Total", Total ?? (object)DBNull.Value);
                p_Total.Direction = ParameterDirection.Output;
                p_Total.DbType = DbType.Int32;
                p_Total.Size = 4;


                // Processing 
                string sqlQuery = $@"EXEC [dbo].[usp_Report_ChannelVerification] @ClientID, @PageSize, @PageNo, @FreeText, @Total OUTPUT";

                //Output Data
                var records = this.usp_Report_ChannelVerification.FromSqlRaw(sqlQuery, p_ClientID, p_PageSize, p_PageNo, p_FreeText, p_Total).ToListAsync();
                records.Wait();
                lst = records.Result;
                //Output Params
                Total = (int?)p_Total.Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
            return lst;
        }

        public async Task<List<usp_GetListByClientIDResult>> usp_GetListByClientIDAsync(int? ClientID, string FreeText, int? TimeZoneOffset)
        {
            //Initialize Result 
            List<usp_GetListByClientIDResult> lst = new List<usp_GetListByClientIDResult>();
            try
            {
                // Parameters
                SqlParameter p_ClientID = new SqlParameter("@ClientID", ClientID ?? (object)DBNull.Value);
                p_ClientID.Direction = ParameterDirection.Input;
                p_ClientID.DbType = DbType.Int32;
                p_ClientID.Size = 4;

                SqlParameter p_FreeText = new SqlParameter("@FreeText", FreeText ?? (object)DBNull.Value);
                p_FreeText.Direction = ParameterDirection.Input;
                p_FreeText.DbType = DbType.String;
                p_FreeText.Size = 100;

                SqlParameter p_TimeZoneOffset = new SqlParameter("@TimeZoneOffset", TimeZoneOffset ?? (object)DBNull.Value);
                p_TimeZoneOffset.Direction = ParameterDirection.Input;
                p_TimeZoneOffset.DbType = DbType.Int32;
                p_TimeZoneOffset.Size = 4;

                // Processing 
                string sqlQuery = $@"EXEC [dbo].[usp_GetListByClientID] @ClientID, @FreeText, @TimeZoneOffset";

                //Output Data
                lst = await this.usp_GetListByClientID.FromSqlRaw(sqlQuery, p_ClientID, p_FreeText, p_TimeZoneOffset).ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
            return lst;
        }


        public List<usp_Report_GetCampaignReportDetailsResult> usp_Report_GetCampaignReportDetailsAsync(int? GroupID, string FreeText, DateTime? FromDate, DateTime? ToDate, int? PageSize, int? PageNo, int? TimeZoneOffset, ref int? Total)
        {
            //Initialize Result 
            List<usp_Report_GetCampaignReportDetailsResult> lst = new List<usp_Report_GetCampaignReportDetailsResult>();
            try
            {
                // Parameters
                SqlParameter p_GroupID = new SqlParameter("@GroupID", GroupID ?? (object)DBNull.Value);
                p_GroupID.Direction = ParameterDirection.Input;
                p_GroupID.DbType = DbType.Int32;
                p_GroupID.Size = 4;

                SqlParameter p_FreeText = new SqlParameter("@FreeText", FreeText ?? (object)DBNull.Value);
                p_FreeText.Direction = ParameterDirection.Input;
                p_FreeText.DbType = DbType.String;
                p_FreeText.Size = 100;

                SqlParameter p_FromDate = new SqlParameter("@FromDate", FromDate ?? (object)DBNull.Value);
                p_FromDate.Direction = ParameterDirection.Input;
                p_FromDate.DbType = DbType.DateTime;
                p_FromDate.Size = 8;

                SqlParameter p_ToDate = new SqlParameter("@ToDate", ToDate ?? (object)DBNull.Value);
                p_ToDate.Direction = ParameterDirection.Input;
                p_ToDate.DbType = DbType.DateTime;
                p_ToDate.Size = 8;

                SqlParameter p_PageSize = new SqlParameter("@PageSize", PageSize ?? (object)DBNull.Value);
                p_PageSize.Direction = ParameterDirection.Input;
                p_PageSize.DbType = DbType.Int32;
                p_PageSize.Size = 4;

                SqlParameter p_PageNo = new SqlParameter("@PageNo", PageNo ?? (object)DBNull.Value);
                p_PageNo.Direction = ParameterDirection.Input;
                p_PageNo.DbType = DbType.Int32;
                p_PageNo.Size = 4;

                SqlParameter p_TimeZoneOffset = new SqlParameter("@TimeZoneOffset", TimeZoneOffset ?? (object)DBNull.Value);
                p_TimeZoneOffset.Direction = ParameterDirection.Input;
                p_TimeZoneOffset.DbType = DbType.Int32;
                p_TimeZoneOffset.Size = 4;

                SqlParameter p_Total = new SqlParameter("@Total", Total ?? (object)DBNull.Value);
                p_Total.Direction = ParameterDirection.Output;
                p_Total.DbType = DbType.Int32;
                p_Total.Size = 4;


                // Processing 
                string sqlQuery = $@"EXEC [dbo].[usp_Report_GetCampaignReportDetails] @GroupID, @FreeText, @FromDate, @ToDate, @PageSize, @PageNo, @TimeZoneOffset, @Total OUTPUT";

                //Output Data
                var records = this.usp_Report_GetCampaignReportDetails.FromSqlRaw(sqlQuery, p_GroupID, p_FreeText, p_FromDate, p_ToDate, p_PageSize, p_PageNo, p_TimeZoneOffset, p_Total).ToListAsync();
                records.Wait();
                lst = records.Result;
                //Output Params
                Total = (int?)p_Total.Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
            return lst;
        }
        public async Task<List<usp_Report_GetCampaignReportHeadingResult>> usp_Report_GetCampaignReportHeadingAsync(int? GroupID)
        {
            //Initialize Result 
            List<usp_Report_GetCampaignReportHeadingResult> lst = new List<usp_Report_GetCampaignReportHeadingResult>();
            try
            {
                // Parameters
                SqlParameter p_GroupID = new SqlParameter("@GroupID", GroupID ?? (object)DBNull.Value);
                p_GroupID.Direction = ParameterDirection.Input;
                p_GroupID.DbType = DbType.Int32;
                p_GroupID.Size = 4;


                // Processing 
                string sqlQuery = $@"EXEC [dbo].[usp_Report_GetCampaignReportHeading] @GroupID";

                //Output Data
                lst = await this.usp_Report_GetCampaignReportHeading.FromSqlRaw(sqlQuery, p_GroupID).ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
            return lst;
        }

        public List<usp_Report_GetGroupReportDetailsResult> usp_Report_GetGroupReportDetailsAsync(int? ClientID, string FreeText, DateTime? FromDate, DateTime? ToDate, int? PageSize, int? PageNo, int? TimeZoneOffset, ref int? Total)
        {
            //Initialize Result 
            List<usp_Report_GetGroupReportDetailsResult> lst = new List<usp_Report_GetGroupReportDetailsResult>();
            try
            {
                // Parameters
                SqlParameter p_ClientID = new SqlParameter("@ClientID", ClientID ?? (object)DBNull.Value);
                p_ClientID.Direction = ParameterDirection.Input;
                p_ClientID.DbType = DbType.Int32;
                p_ClientID.Size = 4;

                SqlParameter p_FreeText = new SqlParameter("@FreeText", FreeText ?? (object)DBNull.Value);
                p_FreeText.Direction = ParameterDirection.Input;
                p_FreeText.DbType = DbType.String;
                p_FreeText.Size = 100;

                SqlParameter p_FromDate = new SqlParameter("@FromDate", FromDate ?? (object)DBNull.Value);
                p_FromDate.Direction = ParameterDirection.Input;
                p_FromDate.DbType = DbType.DateTime;
                p_FromDate.Size = 8;

                SqlParameter p_ToDate = new SqlParameter("@ToDate", ToDate ?? (object)DBNull.Value);
                p_ToDate.Direction = ParameterDirection.Input;
                p_ToDate.DbType = DbType.DateTime;
                p_ToDate.Size = 8;

                SqlParameter p_PageSize = new SqlParameter("@PageSize", PageSize ?? (object)DBNull.Value);
                p_PageSize.Direction = ParameterDirection.Input;
                p_PageSize.DbType = DbType.Int32;
                p_PageSize.Size = 4;

                SqlParameter p_PageNo = new SqlParameter("@PageNo", PageNo ?? (object)DBNull.Value);
                p_PageNo.Direction = ParameterDirection.Input;
                p_PageNo.DbType = DbType.Int32;
                p_PageNo.Size = 4;

                SqlParameter p_TimeZoneOffset = new SqlParameter("@TimeZoneOffset", TimeZoneOffset ?? (object)DBNull.Value);
                p_TimeZoneOffset.Direction = ParameterDirection.Input;
                p_TimeZoneOffset.DbType = DbType.Int32;
                p_TimeZoneOffset.Size = 4;

                SqlParameter p_Total = new SqlParameter("@Total", Total ?? (object)DBNull.Value);
                p_Total.Direction = ParameterDirection.Output;
                p_Total.DbType = DbType.Int32;
                p_Total.Size = 4;


                // Processing 
                string sqlQuery = $@"EXEC [dbo].[usp_Report_GetGroupReportDetails] @ClientID, @FreeText, @FromDate, @ToDate, @PageSize, @PageNo, @TimeZoneOffset, @Total OUTPUT";

                //Output Data
                var records = this.usp_Report_GetGroupReportDetails.FromSqlRaw(sqlQuery, p_ClientID, p_FreeText, p_FromDate, p_ToDate, p_PageSize, p_PageNo, p_TimeZoneOffset, p_Total).ToListAsync();
                records.Wait();
                lst = records.Result;
                //Output Params
                Total = (int?)p_Total.Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
            return lst;
        }

        public async Task<List<usp_Report_GetGroupReportHeadingResult>> usp_Report_GetGroupReportHeadingAsync(int? ClientID)
        {
            //Initialize Result 
            List<usp_Report_GetGroupReportHeadingResult> lst = new List<usp_Report_GetGroupReportHeadingResult>();
            try
            {
                // Parameters
                SqlParameter p_ClientID = new SqlParameter("@ClientID", ClientID ?? (object)DBNull.Value);
                p_ClientID.Direction = ParameterDirection.Input;
                p_ClientID.DbType = DbType.Int32;
                p_ClientID.Size = 4;


                // Processing 
                string sqlQuery = $@"EXEC [dbo].[usp_Report_GetGroupReportHeading] @ClientID";

                //Output Data
                lst = await this.usp_Report_GetGroupReportHeading.FromSqlRaw(sqlQuery, p_ClientID).ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
            return lst;
        }

        public List<usp_Report_GetReadReportDetailsResult> usp_Report_GetReadReportDetailsAsync(int? NoticeID, string FreeText, int? PageSize, int? PageNo, int? TimeZoneOffset, ref int? Total)
        {
            //Initialize Result 
            List<usp_Report_GetReadReportDetailsResult> lst = new List<usp_Report_GetReadReportDetailsResult>();
            try
            {
                // Parameters
                SqlParameter p_NoticeID = new SqlParameter("@NoticeID", NoticeID ?? (object)DBNull.Value);
                p_NoticeID.Direction = ParameterDirection.Input;
                p_NoticeID.DbType = DbType.Int32;
                p_NoticeID.Size = 4;

                SqlParameter p_FreeText = new SqlParameter("@FreeText", FreeText ?? (object)DBNull.Value);
                p_FreeText.Direction = ParameterDirection.Input;
                p_FreeText.DbType = DbType.String;
                p_FreeText.Size = 100;

                SqlParameter p_PageSize = new SqlParameter("@PageSize", PageSize ?? (object)DBNull.Value);
                p_PageSize.Direction = ParameterDirection.Input;
                p_PageSize.DbType = DbType.Int32;
                p_PageSize.Size = 4;

                SqlParameter p_PageNo = new SqlParameter("@PageNo", PageNo ?? (object)DBNull.Value);
                p_PageNo.Direction = ParameterDirection.Input;
                p_PageNo.DbType = DbType.Int32;
                p_PageNo.Size = 4;

                SqlParameter p_TimeZoneOffset = new SqlParameter("@TimeZoneOffset", TimeZoneOffset ?? (object)DBNull.Value);
                p_TimeZoneOffset.Direction = ParameterDirection.Input;
                p_TimeZoneOffset.DbType = DbType.Int32;
                p_TimeZoneOffset.Size = 4;

                SqlParameter p_Total = new SqlParameter("@Total", Total ?? (object)DBNull.Value);
                p_Total.Direction = ParameterDirection.Output;
                p_Total.DbType = DbType.Int32;
                p_Total.Size = 4;


                // Processing 
                string sqlQuery = $@"EXEC [dbo].[usp_Report_GetReadReportDetails] @NoticeID, @FreeText, @PageSize, @PageNo, @TimeZoneOffset, @Total OUTPUT";

                //Output Data
                var records = this.usp_Report_GetReadReportDetails.FromSqlRaw(sqlQuery, p_NoticeID, p_FreeText, p_PageSize, p_PageNo, p_TimeZoneOffset, p_Total).ToListAsync();
                records.Wait();
                lst = records.Result;
                //Output Params
                Total = (int?)p_Total.Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
            return lst;
        }

        public List<usp_Report_GetDNDReportDetailsResult> usp_Report_GetDNDReoprtDetailsAsync(int? ClientID, int? PageSize, int? PageNo, string FreeText, int? TimeZoneOffset, ref int? Total)
        {
            //Initialize Result 
            List<usp_Report_GetDNDReportDetailsResult> lst = new List<usp_Report_GetDNDReportDetailsResult>();
            try
            {
                // Parameters
                SqlParameter p_ClientID = new SqlParameter("@ClientID", ClientID ?? (object)DBNull.Value);
                p_ClientID.Direction = ParameterDirection.Input;
                p_ClientID.DbType = DbType.Int32;
                p_ClientID.Size = 4;

                SqlParameter p_PageSize = new SqlParameter("@PageSize", PageSize ?? (object)DBNull.Value);
                p_PageSize.Direction = ParameterDirection.Input;
                p_PageSize.DbType = DbType.Int32;
                p_PageSize.Size = 4;

                SqlParameter p_PageNo = new SqlParameter("@PageNo", PageNo ?? (object)DBNull.Value);
                p_PageNo.Direction = ParameterDirection.Input;
                p_PageNo.DbType = DbType.Int32;
                p_PageNo.Size = 4;

                SqlParameter p_FreeText = new SqlParameter("@FreeText", FreeText ?? (object)DBNull.Value);
                p_FreeText.Direction = ParameterDirection.Input;
                p_FreeText.DbType = DbType.String;
                p_FreeText.Size = 100;

                SqlParameter p_TimeZoneOffset = new SqlParameter("@TimeZoneOffset", TimeZoneOffset ?? (object)DBNull.Value);
                p_TimeZoneOffset.Direction = ParameterDirection.Input;
                p_TimeZoneOffset.DbType = DbType.Int32;
                p_TimeZoneOffset.Size = 4;

                SqlParameter p_Total = new SqlParameter("@Total", Total ?? (object)DBNull.Value);
                p_Total.Direction = ParameterDirection.Output;
                p_Total.DbType = DbType.Int32;
                p_Total.Size = 4;


                // Processing 
                string sqlQuery = $@"EXEC [dbo].[usp_Report_GetDNDReportDetails] @ClientID, @PageSize, @PageNo, @FreeText,@TimeZoneOffset, @Total OUTPUT";

                //Output Data
                var records = this.usp_Report_GetDNDReportDetails.FromSqlRaw(sqlQuery, p_ClientID, p_PageSize, p_PageNo, p_FreeText, p_TimeZoneOffset, p_Total).ToListAsync();
                records.Wait();
                lst = records.Result;
                //Output Params
                Total = (int?)p_Total.Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
            return lst;
        }
        public async Task<List<usp_GetScheduledNoticeListResult>> usp_GetScheduledNoticeListAsync(DateTime? FromDate, DateTime? ToDate, int? TimeZoneOffset)
        {
            //Initialize Result 
            List<usp_GetScheduledNoticeListResult> lst = new List<usp_GetScheduledNoticeListResult>();
            try
            {
                // Parameters
                SqlParameter p_FromDate = new SqlParameter("@FromDate", FromDate ?? (object)DBNull.Value);
                p_FromDate.Direction = ParameterDirection.Input;
                p_FromDate.DbType = DbType.DateTime;
                p_FromDate.Size = 8;

                SqlParameter p_ToDate = new SqlParameter("@ToDate", ToDate ?? (object)DBNull.Value);
                p_ToDate.Direction = ParameterDirection.Input;
                p_ToDate.DbType = DbType.DateTime;
                p_ToDate.Size = 8;

                SqlParameter p_TimeZoneOffset = new SqlParameter("@TimeZoneOffset", TimeZoneOffset ?? (object)DBNull.Value);
                p_TimeZoneOffset.Direction = ParameterDirection.Input;
                p_TimeZoneOffset.DbType = DbType.Int32;
                p_TimeZoneOffset.Size = 4;

                // Processing 
                string sqlQuery = $@"EXEC [dbo].[usp_GetScheduledNoticeList] @FromDate, @ToDate, @TimeZoneOffset";

                //Output Data
                lst = await this.usp_GetScheduledNoticeList.FromSqlRaw(sqlQuery, p_FromDate, p_ToDate, p_TimeZoneOffset).ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
            return lst;
        }

        public async Task<List<usp_InsertReceiveEmailResult>> usp_InsertReceiveEmailAsync(string EventType, string MsgID, string ReferenceMsgID, string From, string To, DateTime? Timestamp, string Content)
        {
            //Initialize Result 
            List<usp_InsertReceiveEmailResult> lst = new List<usp_InsertReceiveEmailResult>();
            try
            {
                // Parameters
                SqlParameter p_EventType = new SqlParameter("@EventType", EventType ?? (object)DBNull.Value);
                p_EventType.Direction = ParameterDirection.Input;
                p_EventType.DbType = DbType.String;
                p_EventType.Size = 20;

                SqlParameter p_MsgID = new SqlParameter("@MsgID", MsgID ?? (object)DBNull.Value);
                p_MsgID.Direction = ParameterDirection.Input;
                p_MsgID.DbType = DbType.String;
                p_MsgID.Size = 100;

                SqlParameter p_ReferenceMsgID = new SqlParameter("@ReferenceMsgID", ReferenceMsgID ?? (object)DBNull.Value);
                p_ReferenceMsgID.Direction = ParameterDirection.Input;
                p_ReferenceMsgID.DbType = DbType.String;
                p_ReferenceMsgID.Size = 100;

                SqlParameter p_From = new SqlParameter("@From", From ?? (object)DBNull.Value);
                p_From.Direction = ParameterDirection.Input;
                p_From.DbType = DbType.String;
                p_From.Size = 50;

                SqlParameter p_To = new SqlParameter("@To", To ?? (object)DBNull.Value);
                p_To.Direction = ParameterDirection.Input;
                p_To.DbType = DbType.String;
                p_To.Size = 50;

                SqlParameter p_Timestamp = new SqlParameter("@Timestamp", Timestamp ?? (object)DBNull.Value);
                p_Timestamp.Direction = ParameterDirection.Input;
                p_Timestamp.DbType = DbType.DateTime;
                p_Timestamp.Size = 8;

                SqlParameter p_Content = new SqlParameter("@Content", Content ?? (object)DBNull.Value);
                p_Content.Direction = ParameterDirection.Input;
                p_Content.DbType = DbType.String;
                p_Content.Size = 16;


                // Processing 
                string sqlQuery = $@"EXEC [dbo].[usp_InsertReceiveEmail] @EventType, @MsgID, @ReferenceMsgID, @From, @To, @Timestamp, @Content";

                //Output Data
                lst = await this.usp_InsertReceiveEmail.FromSqlRaw(sqlQuery, p_EventType, p_MsgID, p_ReferenceMsgID, p_From, p_To, p_Timestamp, p_Content).ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
            return lst;
        }

        public async Task<List<usp_InsertReceiveSlackResult>> usp_InsertReceiveSlackAsync(string EventType, string MsgID, string ReferenceMsgID, string From, string To, DateTime? Timestamp, string Content)
        {
            //Initialize Result 
            List<usp_InsertReceiveSlackResult> lst = new List<usp_InsertReceiveSlackResult>();
            try
            {
                // Parameters
                SqlParameter p_EventType = new SqlParameter("@EventType", EventType ?? (object)DBNull.Value);
                p_EventType.Direction = ParameterDirection.Input;
                p_EventType.DbType = DbType.String;
                p_EventType.Size = 20;

                SqlParameter p_MsgID = new SqlParameter("@MsgID", MsgID ?? (object)DBNull.Value);
                p_MsgID.Direction = ParameterDirection.Input;
                p_MsgID.DbType = DbType.String;
                p_MsgID.Size = 100;

                SqlParameter p_ReferenceMsgID = new SqlParameter("@ReferenceMsgID", ReferenceMsgID ?? (object)DBNull.Value);
                p_ReferenceMsgID.Direction = ParameterDirection.Input;
                p_ReferenceMsgID.DbType = DbType.String;
                p_ReferenceMsgID.Size = 100;

                SqlParameter p_From = new SqlParameter("@From", From ?? (object)DBNull.Value);
                p_From.Direction = ParameterDirection.Input;
                p_From.DbType = DbType.String;
                p_From.Size = 50;

                SqlParameter p_To = new SqlParameter("@To", To ?? (object)DBNull.Value);
                p_To.Direction = ParameterDirection.Input;
                p_To.DbType = DbType.String;
                p_To.Size = 50;

                SqlParameter p_Timestamp = new SqlParameter("@Timestamp", Timestamp ?? (object)DBNull.Value);
                p_Timestamp.Direction = ParameterDirection.Input;
                p_Timestamp.DbType = DbType.DateTime;
                p_Timestamp.Size = 8;

                SqlParameter p_Content = new SqlParameter("@Content", Content ?? (object)DBNull.Value);
                p_Content.Direction = ParameterDirection.Input;
                p_Content.DbType = DbType.String;
                p_Content.Size = 16;


                // Processing 
                string sqlQuery = $@"EXEC [dbo].[usp_InsertReceiveSlack] @EventType, @MsgID, @ReferenceMsgID, @From, @To, @Timestamp, @Content";

                //Output Data
                lst = await this.usp_InsertReceiveSlack.FromSqlRaw(sqlQuery, p_EventType, p_MsgID, p_ReferenceMsgID, p_From, p_To, p_Timestamp, p_Content).ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
            return lst;
        }

        public async Task<List<usp_GetGroupMappingUsersResult>> usp_GetGroupMappingUsersAsync(int? GroupID, int? ParentID, string NoticeDetail)
        {
            List<usp_GetGroupMappingUsersResult> lst = new List<usp_GetGroupMappingUsersResult>();
            try
            {
                // Parameters
                SqlParameter p_GroupID = new SqlParameter("@GroupID", GroupID ?? (object)DBNull.Value);
                p_GroupID.Direction = ParameterDirection.Input;
                p_GroupID.DbType = DbType.Int32;
                p_GroupID.Size = 4;

                SqlParameter p_ParentID = new SqlParameter("@ParentID", ParentID ?? (object)DBNull.Value);
                p_ParentID.Direction = ParameterDirection.Input;
                p_ParentID.DbType = DbType.Int32;
                p_ParentID.Size = 4;

                SqlParameter p_NoticeDetail = new SqlParameter("@NoticeDetail", NoticeDetail ?? (object)DBNull.Value);
                p_NoticeDetail.Direction = ParameterDirection.Input;
                p_NoticeDetail.DbType = DbType.String;
                p_NoticeDetail.Size = -1;


                // Processing 
                string sqlQuery = $@"EXEC [dbo].[usp_GetGroupMappingUsers] @GroupID, @ParentID, @NoticeDetail";
                //Execution
                lst = await this.usp_GetGroupMappingUsers.FromSqlRaw(sqlQuery, p_GroupID, p_ParentID, p_NoticeDetail).ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
            return lst;
        }

        public List<usp_GetNoticesListResult> usp_GetNoticesListAsync(int? GroupID, Guid? UserID, int? PageSize, int? PageNo, string FreeText, int? TimeZoneOffset, ref int? Total)
        {
            //Initialize Result 
            List<usp_GetNoticesListResult> lst = new List<usp_GetNoticesListResult>();
            try
            {
                // Parameters
                SqlParameter p_GroupID = new SqlParameter("@GroupID", GroupID ?? (object)DBNull.Value);
                p_GroupID.Direction = ParameterDirection.Input;
                p_GroupID.DbType = DbType.Int32;
                p_GroupID.Size = 4;

                SqlParameter p_UserID = new SqlParameter("@UserID", UserID ?? (object)DBNull.Value);
                p_UserID.Direction = ParameterDirection.Input;
                p_UserID.DbType = DbType.Guid;
                p_UserID.Size = 16;

                SqlParameter p_PageSize = new SqlParameter("@PageSize", PageSize ?? (object)DBNull.Value);
                p_PageSize.Direction = ParameterDirection.Input;
                p_PageSize.DbType = DbType.Int32;
                p_PageSize.Size = 4;

                SqlParameter p_PageNo = new SqlParameter("@PageNo", PageNo ?? (object)DBNull.Value);
                p_PageNo.Direction = ParameterDirection.Input;
                p_PageNo.DbType = DbType.Int32;
                p_PageNo.Size = 4;

                SqlParameter p_FreeText = new SqlParameter("@FreeText", FreeText ?? (object)DBNull.Value);
                p_FreeText.Direction = ParameterDirection.Input;
                p_FreeText.DbType = DbType.String;
                p_FreeText.Size = 100;

                SqlParameter p_TimeZoneOffset = new SqlParameter("@TimeZoneOffset", TimeZoneOffset ?? (object)DBNull.Value);
                p_TimeZoneOffset.Direction = ParameterDirection.Input;
                p_TimeZoneOffset.DbType = DbType.Int32;
                p_TimeZoneOffset.Size = 4;

                SqlParameter p_Total = new SqlParameter("@Total", Total ?? (object)DBNull.Value);
                p_Total.Direction = ParameterDirection.Output;
                p_Total.DbType = DbType.Int32;
                p_Total.Size = 4;


                // Processing 
                string sqlQuery = $@"EXEC [dbo].[usp_GetNoticesList] @GroupID, @UserID, @PageSize, @PageNo, @FreeText, @TimeZoneOffset, @Total OUTPUT";

                //Output Data
                var records = this.usp_GetNoticesList.FromSqlRaw(sqlQuery, p_GroupID, p_UserID, p_PageSize, p_PageNo, p_FreeText, p_TimeZoneOffset, p_Total).ToListAsync();
                records.Wait();
                lst = records.Result;
                //Output Params
                Total = (int?)p_Total.Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
            return lst;
        }

        public List<usp_GetNoticesList_RepliesResult> usp_GetNoticesList_RepliesAsync(int? ParentID, Guid? UserID, int? PageSize, int? PageNo, string FreeText, int? TimeZoneOffset, ref int? Total)
        {
            //Initialize Result 
            List<usp_GetNoticesList_RepliesResult> lst = new List<usp_GetNoticesList_RepliesResult>();
            try
            {
                // Parameters
                SqlParameter p_ParentID = new SqlParameter("@ParentID", ParentID ?? (object)DBNull.Value);
                p_ParentID.Direction = ParameterDirection.Input;
                p_ParentID.DbType = DbType.Int32;
                p_ParentID.Size = 4;

                SqlParameter p_UserID = new SqlParameter("@UserID", UserID ?? (object)DBNull.Value);
                p_UserID.Direction = ParameterDirection.Input;
                p_UserID.DbType = DbType.Guid;
                p_UserID.Size = 16;

                SqlParameter p_PageSize = new SqlParameter("@PageSize", PageSize ?? (object)DBNull.Value);
                p_PageSize.Direction = ParameterDirection.Input;
                p_PageSize.DbType = DbType.Int32;
                p_PageSize.Size = 4;

                SqlParameter p_PageNo = new SqlParameter("@PageNo", PageNo ?? (object)DBNull.Value);
                p_PageNo.Direction = ParameterDirection.Input;
                p_PageNo.DbType = DbType.Int32;
                p_PageNo.Size = 4;

                SqlParameter p_FreeText = new SqlParameter("@FreeText", FreeText ?? (object)DBNull.Value);
                p_FreeText.Direction = ParameterDirection.Input;
                p_FreeText.DbType = DbType.String;
                p_FreeText.Size = 100;

                SqlParameter p_TimeZoneOffset = new SqlParameter("@TimeZoneOffset", TimeZoneOffset ?? (object)DBNull.Value);
                p_TimeZoneOffset.Direction = ParameterDirection.Input;
                p_TimeZoneOffset.DbType = DbType.Int32;
                p_TimeZoneOffset.Size = 4;

                SqlParameter p_Total = new SqlParameter("@Total", Total ?? (object)DBNull.Value);
                p_Total.Direction = ParameterDirection.Output;
                p_Total.DbType = DbType.Int32;
                p_Total.Size = 4;


                // Processing 
                string sqlQuery = $@"EXEC [dbo].[usp_GetNoticesList_Replies] @ParentID, @UserID, @PageSize, @PageNo, @FreeText, @TimeZoneOffset, @Total OUTPUT";

                //Output Data
                var records = this.usp_GetNoticesList_Replies.FromSqlRaw(sqlQuery, p_ParentID, p_UserID, p_PageSize, p_PageNo, p_FreeText, p_TimeZoneOffset, p_Total).ToListAsync();
                records.Wait();
                lst = records.Result;
                //Output Params
                Total = (int?)p_Total.Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
            return lst;
        }

        public List<usp_GetUserGroupsResult> usp_GetUserGroupsAsync(Guid? UserID, string FreeText, int? PageSize, int? PageNo, int? TimeZoneOffset, ref int? Total)
        {
            //Initialize Result 
            List<usp_GetUserGroupsResult> lst = new List<usp_GetUserGroupsResult>();
            try
            {
                // Parameters
                SqlParameter p_UserID = new SqlParameter("@UserID", UserID ?? (object)DBNull.Value);
                p_UserID.Direction = ParameterDirection.Input;
                p_UserID.DbType = DbType.Guid;
                p_UserID.Size = 16;

                SqlParameter p_FreeText = new SqlParameter("@FreeText", FreeText ?? (object)DBNull.Value);
                p_FreeText.Direction = ParameterDirection.Input;
                p_FreeText.DbType = DbType.String;
                p_FreeText.Size = 100;

                SqlParameter p_PageSize = new SqlParameter("@PageSize", PageSize ?? (object)DBNull.Value);
                p_PageSize.Direction = ParameterDirection.Input;
                p_PageSize.DbType = DbType.Int32;
                p_PageSize.Size = 4;

                SqlParameter p_PageNo = new SqlParameter("@PageNo", PageNo ?? (object)DBNull.Value);
                p_PageNo.Direction = ParameterDirection.Input;
                p_PageNo.DbType = DbType.Int32;
                p_PageNo.Size = 4;

                SqlParameter p_TimeZoneOffset = new SqlParameter("@TimeZoneOffset", TimeZoneOffset ?? (object)DBNull.Value);
                p_TimeZoneOffset.Direction = ParameterDirection.Input;
                p_TimeZoneOffset.DbType = DbType.Int32;
                p_TimeZoneOffset.Size = 4;

                SqlParameter p_Total = new SqlParameter("@Total", Total ?? (object)DBNull.Value);
                p_Total.Direction = ParameterDirection.Output;
                p_Total.DbType = DbType.Int32;
                p_Total.Size = 4;


                // Processing 
                string sqlQuery = $@"EXEC [dbo].[usp_GetUserGroups] @UserID, @FreeText, @PageSize, @PageNo, @TimeZoneOffset, @Total OUTPUT";

                //Output Data
                var records = this.usp_GetUserGroups.FromSqlRaw(sqlQuery, p_UserID, p_FreeText, p_PageSize, p_PageNo, p_TimeZoneOffset, p_Total).ToListAsync();
                records.Wait();
                lst = records.Result;
                //Output Params
                Total = (int?)p_Total.Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
            return lst;
        }

        public async Task<List<usp_GetUserLoginResult>> usp_GetUserLoginAsync(string UserID, string Password)
        {
            //Initialize Result 
            List<usp_GetUserLoginResult> lst = new List<usp_GetUserLoginResult>();
            try
            {
                // Parameters
                SqlParameter p_UserID = new SqlParameter("@UserID", UserID ?? (object)DBNull.Value);
                p_UserID.Direction = ParameterDirection.Input;
                p_UserID.DbType = DbType.String;
                p_UserID.Size = 40;

                SqlParameter p_Password = new SqlParameter("@Password", Password ?? (object)DBNull.Value);
                p_Password.Direction = ParameterDirection.Input;
                p_Password.DbType = DbType.String;
                p_Password.Size = 100;


                // Processing 
                string sqlQuery = $@"EXEC [dbo].[usp_GetUserLogin] @UserID, @Password";

                //Output Data
                lst = await this.usp_GetUserLogin.FromSqlRaw(sqlQuery, p_UserID, p_Password).ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
            return lst;
        }

        public async Task<List<usp_InserMsgWAResult>> usp_InserMsgWAAsync(string from, string MsgId, string text, DateTime? timestamp, string type)
        {
            //Initialize Result 
            List<usp_InserMsgWAResult> lst = new List<usp_InserMsgWAResult>();
            try
            {
                // Parameters
                SqlParameter p_from = new SqlParameter("@from", from ?? (object)DBNull.Value);
                p_from.Direction = ParameterDirection.Input;
                p_from.DbType = DbType.String;
                p_from.Size = 100;

                SqlParameter p_MsgId = new SqlParameter("@MsgId", MsgId ?? (object)DBNull.Value);
                p_MsgId.Direction = ParameterDirection.Input;
                p_MsgId.DbType = DbType.String;
                p_MsgId.Size = 100;

                SqlParameter p_text = new SqlParameter("@text", text ?? (object)DBNull.Value);
                p_text.Direction = ParameterDirection.Input;
                p_text.DbType = DbType.String;
                p_text.Size = -1;

                SqlParameter p_timestamp = new SqlParameter("@timestamp", timestamp ?? (object)DBNull.Value);
                p_timestamp.Direction = ParameterDirection.Input;
                p_timestamp.DbType = DbType.DateTime;
                p_timestamp.Size = 8;

                SqlParameter p_type = new SqlParameter("@type", type ?? (object)DBNull.Value);
                p_type.Direction = ParameterDirection.Input;
                p_type.DbType = DbType.String;
                p_type.Size = 100;


                // Processing 
                string sqlQuery = $@"EXEC [dbo].[usp_InserMsgWA] @from, @MsgId, @text, @timestamp, @type";

                //Output Data
                lst = await this.usp_InserMsgWA.FromSqlRaw(sqlQuery, p_from, p_MsgId, p_text, p_timestamp, p_type).ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
            return lst;
        }

        public void usp_InsertWA(string RecipientId, string MsgId, string status, DateTime? timestamp, string Type, int? ID)
        {
            try
            {
                // Parameters
                SqlParameter p_RecipientId = new SqlParameter("@RecipientId", RecipientId ?? (object)DBNull.Value);
                p_RecipientId.Direction = ParameterDirection.Input;
                p_RecipientId.DbType = DbType.String;
                p_RecipientId.Size = 20;

                SqlParameter p_MsgId = new SqlParameter("@MsgId", MsgId ?? (object)DBNull.Value);
                p_MsgId.Direction = ParameterDirection.Input;
                p_MsgId.DbType = DbType.String;
                p_MsgId.Size = 100;

                SqlParameter p_status = new SqlParameter("@status", status ?? (object)DBNull.Value);
                p_status.Direction = ParameterDirection.Input;
                p_status.DbType = DbType.String;
                p_status.Size = 50;

                SqlParameter p_timestamp = new SqlParameter("@timestamp", timestamp ?? (object)DBNull.Value);
                p_timestamp.Direction = ParameterDirection.Input;
                p_timestamp.DbType = DbType.DateTime;
                p_timestamp.Size = 8;

                SqlParameter p_Type = new SqlParameter("@Type", Type ?? (object)DBNull.Value);
                p_Type.Direction = ParameterDirection.Input;
                p_Type.DbType = DbType.String;
                p_Type.Size = 50;

                SqlParameter p_ID = new SqlParameter("@ID", ID ?? (object)DBNull.Value);
                p_ID.Direction = ParameterDirection.Input;
                p_ID.DbType = DbType.Int32;
                p_ID.Size = 4;


                // Processing 
                string sqlQuery = $@"EXEC [dbo].[usp_InsertWA] @RecipientId, @MsgId, @status, @timestamp, @Type, @ID";
                //Execution
                this.Database.ExecuteSqlRaw(sqlQuery, p_RecipientId, p_MsgId, p_status, p_timestamp, p_Type, p_ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
        }

        public void usp_UpdateIsRead(string UserId, int? NoticeID, string Type)
        {
            try
            {
                // Parameters
                SqlParameter p_UserId = new SqlParameter("@UserId", UserId ?? (object)DBNull.Value);
                p_UserId.Direction = ParameterDirection.Input;
                p_UserId.DbType = DbType.String;
                p_UserId.Size = 100;

                SqlParameter p_NoticeID = new SqlParameter("@NoticeID", NoticeID ?? (object)DBNull.Value);
                p_NoticeID.Direction = ParameterDirection.Input;
                p_NoticeID.DbType = DbType.Int32;
                p_NoticeID.Size = 4;

                SqlParameter p_Type = new SqlParameter("@Type", Type ?? (object)DBNull.Value);
                p_Type.Direction = ParameterDirection.Input;
                p_Type.DbType = DbType.String;
                p_Type.Size = 20;


                // Processing 
                string sqlQuery = $@"EXEC [dbo].[usp_UpdateIsRead] @UserId, @NoticeID, @Type";
                //Execution
                this.Database.ExecuteSqlRaw(sqlQuery, p_UserId, p_NoticeID, p_Type);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
        }

        public List<usp_User_SelectByGroupIDResult> usp_User_SelectByGroupIDAsync(int? GroupID, int? PageSize, int? PageNo, string FreeText, int? TimeZoneOffset, ref int? Total)
        {
            //Initialize Result 
            List<usp_User_SelectByGroupIDResult> lst = new List<usp_User_SelectByGroupIDResult>();
            try
            {
                // Parameters
                SqlParameter p_GroupID = new SqlParameter("@GroupID", GroupID ?? (object)DBNull.Value);
                p_GroupID.Direction = ParameterDirection.Input;
                p_GroupID.DbType = DbType.Int32;
                p_GroupID.Size = 4;

                SqlParameter p_PageSize = new SqlParameter("@PageSize", PageSize ?? (object)DBNull.Value);
                p_PageSize.Direction = ParameterDirection.Input;
                p_PageSize.DbType = DbType.Int32;
                p_PageSize.Size = 4;

                SqlParameter p_PageNo = new SqlParameter("@PageNo", PageNo ?? (object)DBNull.Value);
                p_PageNo.Direction = ParameterDirection.Input;
                p_PageNo.DbType = DbType.Int32;
                p_PageNo.Size = 4;

                SqlParameter p_FreeText = new SqlParameter("@FreeText", FreeText ?? (object)DBNull.Value);
                p_FreeText.Direction = ParameterDirection.Input;
                p_FreeText.DbType = DbType.String;
                p_FreeText.Size = 100;

                SqlParameter p_TimeZoneOffset = new SqlParameter("@TimeZoneOffset", TimeZoneOffset ?? (object)DBNull.Value);
                p_TimeZoneOffset.Direction = ParameterDirection.Input;
                p_TimeZoneOffset.DbType = DbType.Int32;
                p_TimeZoneOffset.Size = 4;

                SqlParameter p_Total = new SqlParameter("@Total", Total ?? (object)DBNull.Value);
                p_Total.Direction = ParameterDirection.Output;
                p_Total.DbType = DbType.Int32;
                p_Total.Size = 6;

                // Processing 
                string sqlQuery = $@"EXEC [dbo].[usp_User_SelectByGroupID] @GroupID, @PageSize, @PageNo, @FreeText, @TimeZoneOffset, @Total OUTPUT";

                //Output Data
                var records = this.usp_User_SelectByGroupID.FromSqlRaw(sqlQuery, p_GroupID, p_PageSize, p_PageNo, p_FreeText, p_TimeZoneOffset, p_Total).ToListAsync();
                records.Wait();
                lst = records.Result;
                //Output Params
                Total = (int?)p_Total.Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
            return lst;
        }

        public List<usp_User_SelectByListIDResult> usp_User_SelectByListIDAsync(int? ListID, int? PageSize, int? PageNo, string FreeText, int? TimeZoneOffset, ref int? Total)
        {
            //Initialize Result 
            List<usp_User_SelectByListIDResult> lst = new List<usp_User_SelectByListIDResult>();
            try
            {
                // Parameters
                SqlParameter p_ListID = new SqlParameter("@ListID", ListID ?? (object)DBNull.Value);
                p_ListID.Direction = ParameterDirection.Input;
                p_ListID.DbType = DbType.Int32;
                p_ListID.Size = 4;

                SqlParameter p_PageSize = new SqlParameter("@PageSize", PageSize ?? (object)DBNull.Value);
                p_PageSize.Direction = ParameterDirection.Input;
                p_PageSize.DbType = DbType.Int32;
                p_PageSize.Size = 4;

                SqlParameter p_PageNo = new SqlParameter("@PageNo", PageNo ?? (object)DBNull.Value);
                p_PageNo.Direction = ParameterDirection.Input;
                p_PageNo.DbType = DbType.Int32;
                p_PageNo.Size = 4;

                SqlParameter p_FreeText = new SqlParameter("@FreeText", FreeText ?? (object)DBNull.Value);
                p_FreeText.Direction = ParameterDirection.Input;
                p_FreeText.DbType = DbType.String;
                p_FreeText.Size = 100;

                SqlParameter p_TimeZoneOffset = new SqlParameter("@TimeZoneOffset", TimeZoneOffset ?? (object)DBNull.Value);
                p_TimeZoneOffset.Direction = ParameterDirection.Input;
                p_TimeZoneOffset.DbType = DbType.Int32;
                p_TimeZoneOffset.Size = 4;

                SqlParameter p_Total = new SqlParameter("@Total", Total ?? (object)DBNull.Value);
                p_Total.Direction = ParameterDirection.Output;
                p_Total.DbType = DbType.Int32;
                p_Total.Size = 6;

                // Processing 
                string sqlQuery = $@"EXEC [dbo].[usp_User_SelectByListID] @ListID, @PageSize, @PageNo, @FreeText, @TimeZoneOffset, @Total OUTPUT";

                //Output Data
                var records = this.usp_User_SelectByListID.FromSqlRaw(sqlQuery, p_ListID, p_PageSize, p_PageNo, p_FreeText, p_TimeZoneOffset, p_Total).ToListAsync();
                records.Wait();
                lst = records.Result;
                //Output Params
                Total = (int?)p_Total.Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
            return lst;
        }

        public void usp_InsertLinkedHelperData(string member_id, string profile_url, string email, string full_name, string first_name, string last_name, string industry, string address, string birthday, string headline, string current_company, string current_company_custom, string current_company_position, string current_company_custom_position, string organization_1, string organization_url_1, string organization_title_1, string organization_location_1, string organization_website_1, string organization_2, string organization_title_2, string education_1, string education_degree_1, string phone_1, string phone_type_1, string phone_2, string phone_type_2, string messenger_1, string messenger_provider_1, string website_1, string third_party_email_1, string campaign_id, string campaign_name)
        {
            try
            {
                // Parameters
                SqlParameter p_member_id = new SqlParameter("@member_id", member_id ?? (object)DBNull.Value);
                p_member_id.Direction = ParameterDirection.Input;
                p_member_id.DbType = DbType.String;
                p_member_id.Size = 100;

                SqlParameter p_profile_url = new SqlParameter("@profile_url", profile_url ?? (object)DBNull.Value);
                p_profile_url.Direction = ParameterDirection.Input;
                p_profile_url.DbType = DbType.String;
                p_profile_url.Size = 100;

                SqlParameter p_email = new SqlParameter("@email", email ?? (object)DBNull.Value);
                p_email.Direction = ParameterDirection.Input;
                p_email.DbType = DbType.String;
                p_email.Size = 100;

                SqlParameter p_full_name = new SqlParameter("@full_name", full_name ?? (object)DBNull.Value);
                p_full_name.Direction = ParameterDirection.Input;
                p_full_name.DbType = DbType.String;
                p_full_name.Size = 100;

                SqlParameter p_first_name = new SqlParameter("@first_name", first_name ?? (object)DBNull.Value);
                p_first_name.Direction = ParameterDirection.Input;
                p_first_name.DbType = DbType.String;
                p_first_name.Size = 100;

                SqlParameter p_last_name = new SqlParameter("@last_name", last_name ?? (object)DBNull.Value);
                p_last_name.Direction = ParameterDirection.Input;
                p_last_name.DbType = DbType.String;
                p_last_name.Size = 100;

                SqlParameter p_industry = new SqlParameter("@industry", industry ?? (object)DBNull.Value);
                p_industry.Direction = ParameterDirection.Input;
                p_industry.DbType = DbType.String;
                p_industry.Size = 100;

                SqlParameter p_address = new SqlParameter("@address", address ?? (object)DBNull.Value);
                p_address.Direction = ParameterDirection.Input;
                p_address.DbType = DbType.String;
                p_address.Size = 100;

                SqlParameter p_birthday = new SqlParameter("@birthday", birthday ?? (object)DBNull.Value);
                p_birthday.Direction = ParameterDirection.Input;
                p_birthday.DbType = DbType.String;
                p_birthday.Size = 100;

                SqlParameter p_headline = new SqlParameter("@headline", headline ?? (object)DBNull.Value);
                p_headline.Direction = ParameterDirection.Input;
                p_headline.DbType = DbType.String;
                p_headline.Size = 100;

                SqlParameter p_current_company = new SqlParameter("@current_company", current_company ?? (object)DBNull.Value);
                p_current_company.Direction = ParameterDirection.Input;
                p_current_company.DbType = DbType.String;
                p_current_company.Size = 100;

                SqlParameter p_current_company_custom = new SqlParameter("@current_company_custom", current_company_custom ?? (object)DBNull.Value);
                p_current_company_custom.Direction = ParameterDirection.Input;
                p_current_company_custom.DbType = DbType.String;
                p_current_company_custom.Size = 100;

                SqlParameter p_current_company_position = new SqlParameter("@current_company_position", current_company_position ?? (object)DBNull.Value);
                p_current_company_position.Direction = ParameterDirection.Input;
                p_current_company_position.DbType = DbType.String;
                p_current_company_position.Size = 100;

                SqlParameter p_current_company_custom_position = new SqlParameter("@current_company_custom_position", current_company_custom_position ?? (object)DBNull.Value);
                p_current_company_custom_position.Direction = ParameterDirection.Input;
                p_current_company_custom_position.DbType = DbType.String;
                p_current_company_custom_position.Size = 100;

                SqlParameter p_organization_1 = new SqlParameter("@organization_1", organization_1 ?? (object)DBNull.Value);
                p_organization_1.Direction = ParameterDirection.Input;
                p_organization_1.DbType = DbType.String;
                p_organization_1.Size = 100;

                SqlParameter p_organization_url_1 = new SqlParameter("@organization_url_1", organization_url_1 ?? (object)DBNull.Value);
                p_organization_url_1.Direction = ParameterDirection.Input;
                p_organization_url_1.DbType = DbType.String;
                p_organization_url_1.Size = 100;

                SqlParameter p_organization_title_1 = new SqlParameter("@organization_title_1", organization_title_1 ?? (object)DBNull.Value);
                p_organization_title_1.Direction = ParameterDirection.Input;
                p_organization_title_1.DbType = DbType.String;
                p_organization_title_1.Size = 100;

                SqlParameter p_organization_location_1 = new SqlParameter("@organization_location_1", organization_location_1 ?? (object)DBNull.Value);
                p_organization_location_1.Direction = ParameterDirection.Input;
                p_organization_location_1.DbType = DbType.String;
                p_organization_location_1.Size = 100;

                SqlParameter p_organization_website_1 = new SqlParameter("@organization_website_1", organization_website_1 ?? (object)DBNull.Value);
                p_organization_website_1.Direction = ParameterDirection.Input;
                p_organization_website_1.DbType = DbType.String;
                p_organization_website_1.Size = 100;

                SqlParameter p_organization_2 = new SqlParameter("@organization_2", organization_2 ?? (object)DBNull.Value);
                p_organization_2.Direction = ParameterDirection.Input;
                p_organization_2.DbType = DbType.String;
                p_organization_2.Size = 100;

                SqlParameter p_organization_title_2 = new SqlParameter("@organization_title_2", organization_title_2 ?? (object)DBNull.Value);
                p_organization_title_2.Direction = ParameterDirection.Input;
                p_organization_title_2.DbType = DbType.String;
                p_organization_title_2.Size = 100;

                SqlParameter p_education_1 = new SqlParameter("@education_1", education_1 ?? (object)DBNull.Value);
                p_education_1.Direction = ParameterDirection.Input;
                p_education_1.DbType = DbType.String;
                p_education_1.Size = 100;

                SqlParameter p_education_degree_1 = new SqlParameter("@education_degree_1", education_degree_1 ?? (object)DBNull.Value);
                p_education_degree_1.Direction = ParameterDirection.Input;
                p_education_degree_1.DbType = DbType.String;
                p_education_degree_1.Size = 100;

                SqlParameter p_phone_1 = new SqlParameter("@phone_1", phone_1 ?? (object)DBNull.Value);
                p_phone_1.Direction = ParameterDirection.Input;
                p_phone_1.DbType = DbType.String;
                p_phone_1.Size = 100;

                SqlParameter p_phone_type_1 = new SqlParameter("@phone_type_1", phone_type_1 ?? (object)DBNull.Value);
                p_phone_type_1.Direction = ParameterDirection.Input;
                p_phone_type_1.DbType = DbType.String;
                p_phone_type_1.Size = 100;

                SqlParameter p_phone_2 = new SqlParameter("@phone_2", phone_2 ?? (object)DBNull.Value);
                p_phone_2.Direction = ParameterDirection.Input;
                p_phone_2.DbType = DbType.String;
                p_phone_2.Size = 100;

                SqlParameter p_phone_type_2 = new SqlParameter("@phone_type_2", phone_type_2 ?? (object)DBNull.Value);
                p_phone_type_2.Direction = ParameterDirection.Input;
                p_phone_type_2.DbType = DbType.String;
                p_phone_type_2.Size = 100;

                SqlParameter p_messenger_1 = new SqlParameter("@messenger_1", messenger_1 ?? (object)DBNull.Value);
                p_messenger_1.Direction = ParameterDirection.Input;
                p_messenger_1.DbType = DbType.String;
                p_messenger_1.Size = 100;

                SqlParameter p_messenger_provider_1 = new SqlParameter("@messenger_provider_1", messenger_provider_1 ?? (object)DBNull.Value);
                p_messenger_provider_1.Direction = ParameterDirection.Input;
                p_messenger_provider_1.DbType = DbType.String;
                p_messenger_provider_1.Size = 100;

                SqlParameter p_website_1 = new SqlParameter("@website_1", website_1 ?? (object)DBNull.Value);
                p_website_1.Direction = ParameterDirection.Input;
                p_website_1.DbType = DbType.String;
                p_website_1.Size = 100;

                SqlParameter p_third_party_email_1 = new SqlParameter("@third_party_email_1", third_party_email_1 ?? (object)DBNull.Value);
                p_third_party_email_1.Direction = ParameterDirection.Input;
                p_third_party_email_1.DbType = DbType.String;
                p_third_party_email_1.Size = 100;

                SqlParameter p_campaign_id = new SqlParameter("@campaign_id", campaign_id ?? (object)DBNull.Value);
                p_campaign_id.Direction = ParameterDirection.Input;
                p_campaign_id.DbType = DbType.String;
                p_campaign_id.Size = 100;

                SqlParameter p_campaign_name = new SqlParameter("@campaign_name", campaign_name ?? (object)DBNull.Value);
                p_campaign_name.Direction = ParameterDirection.Input;
                p_campaign_name.DbType = DbType.String;
                p_campaign_name.Size = 100;


                // Processing 
                string sqlQuery = $@"EXEC [dbo].[usp_InsertLinkedHelperData] @member_id, @profile_url, @email, @full_name, @first_name, @last_name, @industry, @address, @birthday, @headline, @current_company, @current_company_custom, @current_company_position, @current_company_custom_position, @organization_1, @organization_url_1, @organization_title_1, @organization_location_1, @organization_website_1, @organization_2, @organization_title_2, @education_1, @education_degree_1, @phone_1, @phone_type_1, @phone_2, @phone_type_2, @messenger_1, @messenger_provider_1, @website_1, @third_party_email_1, @campaign_id, @campaign_name";
                //Execution
                this.Database.ExecuteSqlRaw(sqlQuery, p_member_id, p_profile_url, p_email, p_full_name, p_first_name, p_last_name, p_industry, p_address, p_birthday, p_headline, p_current_company, p_current_company_custom, p_current_company_position, p_current_company_custom_position, p_organization_1, p_organization_url_1, p_organization_title_1, p_organization_location_1, p_organization_website_1, p_organization_2, p_organization_title_2, p_education_1, p_education_degree_1, p_phone_1, p_phone_type_1, p_phone_2, p_phone_type_2, p_messenger_1, p_messenger_provider_1, p_website_1, p_third_party_email_1, p_campaign_id, p_campaign_name);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
        }

        public void usp_InsertSchedularData(string NoticeTitle, string NoticeDetailHTML, bool IsSms, bool IsEmail, bool IsWhatsapp, bool IsFacebook, bool IsSlack, bool IsInstagram, bool IsViber, bool IsLine, bool IsWeChat, bool IsPushNotification, bool IsBroadcastToAllChannel, int GroupId, int ParentId, DateTime ScheduleTime, string UserName, string Password, string FileName, int FileType, string TemplateId)
        {
            try
            {
                // Parameters
                SqlParameter p_noticeTitle = new SqlParameter("@NoticeTitle", NoticeTitle ?? (object)DBNull.Value);
                p_noticeTitle.Direction = ParameterDirection.Input;
                p_noticeTitle.DbType = DbType.String;

                SqlParameter p_noticeDetailHTML = new SqlParameter("@NoticeDetailHTML", NoticeDetailHTML ?? (object)DBNull.Value);
                p_noticeDetailHTML.Direction = ParameterDirection.Input;
                p_noticeDetailHTML.DbType = DbType.String;

                SqlParameter p_IsSms = new SqlParameter("@IsSms", IsSms);
                p_IsSms.Direction = ParameterDirection.Input;
                p_IsSms.DbType = DbType.Boolean;

                SqlParameter p_IsEmail = new SqlParameter("@IsEmail", IsEmail);
                p_IsEmail.Direction = ParameterDirection.Input;
                p_IsEmail.DbType = DbType.Boolean;

                SqlParameter p_IsWhatsapp = new SqlParameter("@IsWhatsapp", IsWhatsapp);
                p_IsWhatsapp.Direction = ParameterDirection.Input;
                p_IsWhatsapp.DbType = DbType.Boolean;

                SqlParameter p_IsFacebook = new SqlParameter("@IsFacebook", IsFacebook);
                p_IsFacebook.Direction = ParameterDirection.Input;
                p_IsFacebook.DbType = DbType.Boolean;

                SqlParameter p_IsSlack = new SqlParameter("@IsSlack", IsSlack);
                p_IsSlack.Direction = ParameterDirection.Input;
                p_IsSlack.DbType = DbType.Boolean;

                SqlParameter p_IsInstagram = new SqlParameter("@IsInstagram", IsInstagram);
                p_IsInstagram.Direction = ParameterDirection.Input;
                p_IsInstagram.DbType = DbType.Boolean;

                SqlParameter p_IsViber = new SqlParameter("@IsViber", IsViber);
                p_IsViber.Direction = ParameterDirection.Input;
                p_IsViber.DbType = DbType.Boolean;

                SqlParameter p_IsLine = new SqlParameter("@IsLine", IsLine);
                p_IsLine.Direction = ParameterDirection.Input;
                p_IsLine.DbType = DbType.Boolean;

                SqlParameter p_IsWeChat = new SqlParameter("@IsWeChat", IsWeChat);
                p_IsWeChat.Direction = ParameterDirection.Input;
                p_IsWeChat.DbType = DbType.Boolean;

                SqlParameter p_IsPushNotification = new SqlParameter("@IsPushNotification", IsPushNotification);
                p_IsPushNotification.Direction = ParameterDirection.Input;
                p_IsPushNotification.DbType = DbType.Boolean;


                SqlParameter p_IsBroadcastToAllChannel = new SqlParameter("@IsBroadcastToAllChannel", IsBroadcastToAllChannel);
                p_IsBroadcastToAllChannel.Direction = ParameterDirection.Input;
                p_IsBroadcastToAllChannel.DbType = DbType.Boolean;

                SqlParameter p_groupId = new SqlParameter("@GroupId", GroupId);
                p_groupId.Direction = ParameterDirection.Input;
                p_groupId.DbType = DbType.Int32;

                SqlParameter p_parentId = new SqlParameter("@ParentId", ParentId);
                p_parentId.Direction = ParameterDirection.Input;
                p_parentId.DbType = DbType.Int32;

                SqlParameter p_scheduleTime = new SqlParameter("@ScheduleTime", ScheduleTime);
                p_scheduleTime.Direction = ParameterDirection.Input;
                p_scheduleTime.DbType = DbType.DateTime2;

                SqlParameter p_userName = new SqlParameter("@UserName", UserName);
                p_noticeTitle.Direction = ParameterDirection.Input;
                p_noticeTitle.DbType = DbType.String;

                SqlParameter p_password = new SqlParameter("@Password", Password);
                p_noticeTitle.Direction = ParameterDirection.Input;
                p_noticeTitle.DbType = DbType.String;

                SqlParameter p_fileName = new SqlParameter("@FileName", FileName ?? (object)DBNull.Value);
                p_fileName.Direction = ParameterDirection.Input;
                p_fileName.DbType = DbType.String;

                SqlParameter p_fileType = new SqlParameter("@FileType", FileType);
                p_fileType.Direction = ParameterDirection.Input;
                p_fileType.DbType = DbType.Int32;

                SqlParameter p_templateId = new SqlParameter("@TemplateId", TemplateId ?? (object)DBNull.Value);
                p_templateId.Direction = ParameterDirection.Input;
                p_noticeTitle.DbType = DbType.String;

                // Processing 
                string sqlQuery = $@"EXEC [dbo].[InsertScheduleData] @NoticeTitle, @NoticeDetailHTML, @IsSms, @IsEmail, @IsWhatsapp, @IsFacebook, @IsSlack, @IsInstagram, @IsViber, @IsLine, @IsWeChat, @IsPushNotification, @IsBroadcastToAllChannel, @GroupId, @ParentId, @ScheduleTime, @UserName, @Password, @FileName, @FileType, @TemplateId";
                //Execution
                this.Database.ExecuteSqlRaw(sqlQuery, p_noticeTitle, p_noticeDetailHTML, p_IsSms, p_IsEmail, p_IsWhatsapp, p_IsFacebook, p_IsSlack, p_IsInstagram, p_IsViber, p_IsLine, p_IsWeChat, p_IsPushNotification, p_IsBroadcastToAllChannel, p_groupId, p_parentId, p_scheduleTime, p_userName, p_password, p_fileName, p_fileType, p_templateId);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
        }

        public void usp_RemoveSchedularData(string schedularIds)
        {
            try
            {
                // Parameters
                SqlParameter p_schedularIds = new SqlParameter("@SchedularIds", schedularIds ?? (object)DBNull.Value);
                p_schedularIds.Direction = ParameterDirection.Input;
                p_schedularIds.DbType = DbType.String;

                // Processing 
                string sqlQuery = $@"EXEC [dbo].[RemoveSchedularData] @SchedularIds";
                //Execution
                this.Database.ExecuteSqlRaw(sqlQuery, p_schedularIds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<usp_GetSchedularDataResult>> usp_GetSchedularDataResultAsync()
        {
            //Initialize Result 
            List<usp_GetSchedularDataResult> lst = new List<usp_GetSchedularDataResult>();
            try
            {
                // Processing 
                string sqlQuery = $@"EXEC [dbo].[GetScheduleData]";

                //Output Data
                lst = await this.usp_GetSchedularData.FromSqlRaw(sqlQuery).ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
            return lst;
        }

        public List<usp_GetSchedularDataResult> usp_GetSchedulePagedDataResultAsync(DateTime? startDate, DateTime? endDate, int? pageSize, int? pageNo, ref int? total)
        {
            //Initialize Result 
            List<usp_GetSchedularDataResult> lst = new List<usp_GetSchedularDataResult>();
            try
            {
                // Parameters
                SqlParameter p_startDate = new SqlParameter("@StartDate", startDate ?? (object)DBNull.Value);
                p_startDate.Direction = ParameterDirection.Input;
                p_startDate.DbType = DbType.DateTime;

                SqlParameter p_endDate = new SqlParameter("@EndDate", endDate ?? (object)DBNull.Value);
                p_endDate.Direction = ParameterDirection.Input;
                p_endDate.DbType = DbType.DateTime;

                SqlParameter p_PageSize = new SqlParameter("@PageSize", pageSize ?? (object)DBNull.Value);
                p_PageSize.Direction = ParameterDirection.Input;
                p_PageSize.DbType = DbType.Int32;

                SqlParameter p_PageNo = new SqlParameter("@PageNo", pageNo ?? (object)DBNull.Value);
                p_PageNo.Direction = ParameterDirection.Input;
                p_PageNo.DbType = DbType.Int32;

                SqlParameter p_Total = new SqlParameter("@Total", total ?? (object)DBNull.Value);
                p_Total.Direction = ParameterDirection.Output;
                p_Total.DbType = DbType.Int32;
                p_Total.Size = 6;

                // Processing 
                string sqlQuery = $@"EXEC [dbo].[GetScheduledPagedData] @StartDate, @EndDate, @PageSize, @PageNo, @Total OUTPUT";

                //Output Data
                var records = this.usp_GetSchedularData.FromSqlRaw(sqlQuery, p_startDate, p_endDate, p_PageSize, p_PageNo, p_Total).ToListAsync();
                records.Wait();
                lst = records.Result;
                //Output Params
                total = (int?)p_Total.Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
            return lst;
        }


        public void usp_InsertTemplateData(string name, string body, string Type, bool IsSms, bool IsEmail, bool IsWhatsapp, bool IsFacebook, bool IsSlack, bool IsInstagram, bool IsViber, bool IsLine, bool IsWeChat, bool IsPushNotification)
        {
            try
            {
                // Parameters
                SqlParameter p_name = new SqlParameter("@Name", name ?? (object)DBNull.Value);
                p_name.Direction = ParameterDirection.Input;
                p_name.DbType = DbType.String;

                SqlParameter p_body = new SqlParameter("@Body", body ?? (object)DBNull.Value);
                p_body.Direction = ParameterDirection.Input;
                p_body.DbType = DbType.String;

                SqlParameter p_type = new SqlParameter("@Type", Type ?? (object)DBNull.Value);
                p_type.Direction = ParameterDirection.Input;
                p_type.DbType = DbType.String;

                SqlParameter p_IsSms = new SqlParameter("@IsSms", IsSms);
                p_IsSms.Direction = ParameterDirection.Input;
                p_IsSms.DbType = DbType.Boolean;

                SqlParameter p_IsEmail = new SqlParameter("@IsEmail", IsEmail);
                p_IsEmail.Direction = ParameterDirection.Input;
                p_IsEmail.DbType = DbType.Boolean;

                SqlParameter p_IsWhatsapp = new SqlParameter("@IsWhatsapp", IsWhatsapp);
                p_IsWhatsapp.Direction = ParameterDirection.Input;
                p_IsWhatsapp.DbType = DbType.Boolean;

                SqlParameter p_IsFacebook = new SqlParameter("@IsFacebook", IsFacebook);
                p_IsFacebook.Direction = ParameterDirection.Input;
                p_IsFacebook.DbType = DbType.Boolean;

                SqlParameter p_IsSlack = new SqlParameter("@IsSlack", IsSlack);
                p_IsSlack.Direction = ParameterDirection.Input;
                p_IsSlack.DbType = DbType.Boolean;

                SqlParameter p_IsInstagram = new SqlParameter("@IsInstagram", IsInstagram);
                p_IsInstagram.Direction = ParameterDirection.Input;
                p_IsInstagram.DbType = DbType.Boolean;

                SqlParameter p_IsViber = new SqlParameter("@IsViber", IsViber);
                p_IsViber.Direction = ParameterDirection.Input;
                p_IsViber.DbType = DbType.Boolean;

                SqlParameter p_IsLine = new SqlParameter("@IsLine", IsLine);
                p_IsLine.Direction = ParameterDirection.Input;
                p_IsLine.DbType = DbType.Boolean;

                SqlParameter p_IsWeChat = new SqlParameter("@IsWeChat", IsWeChat);
                p_IsWeChat.Direction = ParameterDirection.Input;
                p_IsWeChat.DbType = DbType.Boolean;

                SqlParameter p_IsPushNotification = new SqlParameter("@IsPushNotification", IsPushNotification);
                p_IsPushNotification.Direction = ParameterDirection.Input;
                p_IsPushNotification.DbType = DbType.Boolean;


                // Processing 
                string sqlQuery = $@"EXEC [dbo].[AddTemplate] @Name, @Body, @Type, @IsSms, @IsEmail, @IsWhatsapp, @IsFacebook, @IsSlack, @IsInstagram, @IsViber, @IsLine, @IsWeChat, @IsPushNotification";
                //Execution
                this.Database.ExecuteSqlRaw(sqlQuery, p_name, p_body, p_type, p_IsSms, p_IsEmail, p_IsWhatsapp, p_IsFacebook, p_IsSlack, p_IsInstagram, p_IsViber, p_IsLine, p_IsWeChat, p_IsPushNotification);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
        }

        public void usp_UpdateTemplateData(int id, string name, string body, string Type, bool IsSms, bool IsEmail, bool IsWhatsapp, bool IsFacebook, bool IsSlack, bool IsInstagram, bool IsViber, bool IsLine, bool IsWeChat, bool IsPushNotification)
        {
            try
            {
                // Parameters
                SqlParameter p_id = new SqlParameter("@Id", id);
                p_id.Direction = ParameterDirection.Input;
                p_id.DbType = DbType.Int32;

                SqlParameter p_name = new SqlParameter("@Name", name ?? (object)DBNull.Value);
                p_name.Direction = ParameterDirection.Input;
                p_name.DbType = DbType.String;

                SqlParameter p_body = new SqlParameter("@Body", body ?? (object)DBNull.Value);
                p_body.Direction = ParameterDirection.Input;
                p_body.DbType = DbType.String;

                SqlParameter p_type = new SqlParameter("@Type", Type ?? (object)DBNull.Value);
                p_type.Direction = ParameterDirection.Input;
                p_type.DbType = DbType.String;

                SqlParameter p_IsSms = new SqlParameter("@IsSms", IsSms);
                p_IsSms.Direction = ParameterDirection.Input;
                p_IsSms.DbType = DbType.Boolean;

                SqlParameter p_IsEmail = new SqlParameter("@IsEmail", IsEmail);
                p_IsEmail.Direction = ParameterDirection.Input;
                p_IsEmail.DbType = DbType.Boolean;

                SqlParameter p_IsWhatsapp = new SqlParameter("@IsWhatsapp", IsWhatsapp);
                p_IsWhatsapp.Direction = ParameterDirection.Input;
                p_IsWhatsapp.DbType = DbType.Boolean;

                SqlParameter p_IsFacebook = new SqlParameter("@IsFacebook", IsFacebook);
                p_IsFacebook.Direction = ParameterDirection.Input;
                p_IsFacebook.DbType = DbType.Boolean;

                SqlParameter p_IsSlack = new SqlParameter("@IsSlack", IsSlack);
                p_IsSlack.Direction = ParameterDirection.Input;
                p_IsSlack.DbType = DbType.Boolean;

                SqlParameter p_IsInstagram = new SqlParameter("@IsInstagram", IsInstagram);
                p_IsInstagram.Direction = ParameterDirection.Input;
                p_IsInstagram.DbType = DbType.Boolean;

                SqlParameter p_IsViber = new SqlParameter("@IsViber", IsViber);
                p_IsViber.Direction = ParameterDirection.Input;
                p_IsViber.DbType = DbType.Boolean;

                SqlParameter p_IsLine = new SqlParameter("@IsLine", IsLine);
                p_IsLine.Direction = ParameterDirection.Input;
                p_IsLine.DbType = DbType.Boolean;

                SqlParameter p_IsWeChat = new SqlParameter("@IsWeChat", IsWeChat);
                p_IsWeChat.Direction = ParameterDirection.Input;
                p_IsWeChat.DbType = DbType.Boolean;

                SqlParameter p_IsPushNotification = new SqlParameter("@IsPushNotification", IsPushNotification);
                p_IsPushNotification.Direction = ParameterDirection.Input;
                p_IsPushNotification.DbType = DbType.Boolean;


                // Processing 
                string sqlQuery = $@"EXEC [dbo].[UpdateTemplate] @Id, @Name, @Body, @Type, @IsSms, @IsEmail, @IsWhatsapp, @IsFacebook, @IsSlack, @IsInstagram, @IsViber, @IsLine, @IsWeChat, @IsPushNotification";
                //Execution
                this.Database.ExecuteSqlRaw(sqlQuery, p_id, p_name, p_body, p_type, p_IsSms, p_IsEmail, p_IsWhatsapp, p_IsFacebook, p_IsSlack, p_IsInstagram, p_IsViber, p_IsLine, p_IsWeChat, p_IsPushNotification);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
        }

        public void usp_RemoveTemplateData(int id)
        {
            try
            {
                // Parameters
                SqlParameter p_id = new SqlParameter("@Id", id);
                p_id.Direction = ParameterDirection.Input;
                p_id.DbType = DbType.Int32;

                // Processing 
                string sqlQuery = $@"EXEC [dbo].[RemoveTemplate] @Id";
                //Execution
                this.Database.ExecuteSqlRaw(sqlQuery, p_id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<usp_GetTemplateDataResult> usp_GetTemplateDataResultAsync(string title, string type, int? pageSize, int? pageNo, ref int? total)
        {
            //Initialize Result 
            List<usp_GetTemplateDataResult> lst = new List<usp_GetTemplateDataResult>();
            try
            {
                // Parameters
                SqlParameter p_title = new SqlParameter("@Name", title ?? (object)DBNull.Value);
                p_title.Direction = ParameterDirection.Input;
                p_title.DbType = DbType.String;

                SqlParameter p_type = new SqlParameter("@Type", type ?? (object)DBNull.Value);
                p_type.Direction = ParameterDirection.Input;
                p_type.DbType = DbType.String;

                SqlParameter p_PageSize = new SqlParameter("@PageSize", pageSize ?? (object)DBNull.Value);
                p_PageSize.Direction = ParameterDirection.Input;
                p_PageSize.DbType = DbType.Int32;

                SqlParameter p_PageNo = new SqlParameter("@PageNo", pageNo ?? (object)DBNull.Value);
                p_PageNo.Direction = ParameterDirection.Input;
                p_PageNo.DbType = DbType.Int32;

                SqlParameter p_Total = new SqlParameter("@Total", total ?? (object)DBNull.Value);
                p_Total.Direction = ParameterDirection.Output;
                p_Total.DbType = DbType.Int32;
                p_Total.Size = 6;

                // Processing 
                string sqlQuery = $@"EXEC [dbo].[GetTemplate] @Name, @Type, @PageSize, @PageNo, @Total OUTPUT";

                //Output Data
                var records = this.usp_GetTemplateData.FromSqlRaw(sqlQuery, p_title, p_type, p_PageSize, p_PageNo, p_Total).ToListAsync();
                records.Wait();
                lst = records.Result;
                //Output Params
                total = (int?)p_Total.Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
            return lst;
        }

        public List<usp_GetUserIdsFromGroupResult> usp_GetUserIdsFromGroupResultAsync(int groupId)
        {
            //Initialize Result 
            List<usp_GetUserIdsFromGroupResult> lst = new List<usp_GetUserIdsFromGroupResult>();
            try
            {
                // Parameters

                SqlParameter p_groupId = new SqlParameter("@GroupId", groupId);
                p_groupId.Direction = ParameterDirection.Input;
                p_groupId.DbType = DbType.Int32;

                // Processing 
                string sqlQuery = $@"EXEC [dbo].[GetUserIdsFromGroup] @GroupId";

                //Output Data
                //lst = await this.usp_GetUserIdsFromGroupData.FromSqlRaw(sqlQuery).ToListAsync();

                var records = this.usp_GetUserIdsFromGroupData.FromSqlRaw(sqlQuery, p_groupId).ToListAsync();
                records.Wait();
                lst = records.Result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
            return lst;
        }

        public async Task<List<usp_GetNoticeDetailsForHubResult>> usp_GetNoticeDetailsForHubAsync(int noticeID)
        {
            //Initialize Result 
            List<usp_GetNoticeDetailsForHubResult> lst = new List<usp_GetNoticeDetailsForHubResult>();
            try
            {
                // Parameters
                SqlParameter p_noticeID = new SqlParameter("@NoticeID", noticeID);
                p_noticeID.Direction = ParameterDirection.Input;
                p_noticeID.DbType = DbType.Int32;

                // Processing 
                string sqlQuery = $@"EXEC [dbo].[GetNoticeDetailsForHub] @NoticeID";

                //Output Data
                var records = this.usp_GetNoticeDetailsForHubData.FromSqlRaw(sqlQuery, p_noticeID).ToListAsync();
                records.Wait();
                lst = records.Result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
            return lst;
        }

        public async Task<List<usp_ChannelPredictionFileResult>> usp_Report_GetChannelPredictionDataAsync()
        {
            //Initialize Result 
            List<usp_ChannelPredictionFileResult> lst = new List<usp_ChannelPredictionFileResult>();
            try
            {
                // Processing 
                string sqlQuery = $@"EXEC [dbo].[GenerateChannelPredictionData]";

                //Output Data
                lst = await this.usp_ChannelPredictionFileData.FromSqlRaw(sqlQuery).ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
            return lst;
        }

        public List<usp_GetChannelPredictionResult> usp_GetChannelPredictionAsync(string slot, string search, int? pageSize, int? pageNo, ref int? total, ref string MostPrefferedChannel, ref string SecondMostPrefferedChannel, ref string ThirdMostPrefferedChannel)
        {
            //Initialize Result 
            List<usp_GetChannelPredictionResult> lst = new List<usp_GetChannelPredictionResult>();
            try
            {
                // Parameters
                SqlParameter p_slot = new SqlParameter("@Slot", slot ?? (object)DBNull.Value);
                p_slot.Direction = ParameterDirection.Input;

                SqlParameter p_search = new SqlParameter("@Search", search ?? (object)DBNull.Value);
                p_search.Direction = ParameterDirection.Input;

                SqlParameter p_PageSize = new SqlParameter("@PageSize", pageSize ?? (object)DBNull.Value);
                p_PageSize.Direction = ParameterDirection.Input;
                p_PageSize.DbType = DbType.Int32;

                SqlParameter p_PageNo = new SqlParameter("@PageNo", pageNo ?? (object)DBNull.Value);
                p_PageNo.Direction = ParameterDirection.Input;
                p_PageNo.DbType = DbType.Int32;

                SqlParameter p_Total = new SqlParameter("@Total", total ?? (object)DBNull.Value);
                p_Total.Direction = ParameterDirection.Output;
                p_Total.DbType = DbType.Int32;
                p_Total.Size = 6;

                SqlParameter p_MostPrefferedChannel = new SqlParameter("@MostPrefferedChannel", MostPrefferedChannel ?? (object)DBNull.Value);
                p_MostPrefferedChannel.Direction = ParameterDirection.Output;
                p_MostPrefferedChannel.DbType = DbType.String;
                p_MostPrefferedChannel.Size = 30;

                SqlParameter p_SecondMostPrefferedChannel = new SqlParameter("@SecondMostPrefferedChannel", SecondMostPrefferedChannel ?? (object)DBNull.Value);
                p_SecondMostPrefferedChannel.Direction = ParameterDirection.Output;
                p_SecondMostPrefferedChannel.DbType = DbType.String;
                p_SecondMostPrefferedChannel.Size = 30;

                SqlParameter p_ThirdMostPrefferedChannel = new SqlParameter("@ThirdMostPrefferedChannel", ThirdMostPrefferedChannel ?? (object)DBNull.Value);
                p_ThirdMostPrefferedChannel.Direction = ParameterDirection.Output;
                p_ThirdMostPrefferedChannel.DbType = DbType.String;
                p_ThirdMostPrefferedChannel.Size = 30;

                // Processing 
                string sqlQuery = $@"EXEC [dbo].[GetChannelPredictionData] @Slot, @Search, @PageSize, @PageNo, @Total OUTPUT, @MostPrefferedChannel OUTPUT, @SecondMostPrefferedChannel OUTPUT, @ThirdMostPrefferedChannel OUTPUT";

                //Output Data
                var records = this.usp_GetChannelPredictionData.FromSqlRaw(sqlQuery, p_slot, p_search, p_PageSize, p_PageNo, p_Total, p_MostPrefferedChannel, p_SecondMostPrefferedChannel, p_ThirdMostPrefferedChannel).ToListAsync();
                records.Wait();
                lst = records.Result;
                //Output Params
                total = (int?)p_Total.Value;
                MostPrefferedChannel = Convert.ToString(p_MostPrefferedChannel.Value);
                SecondMostPrefferedChannel = Convert.ToString(p_SecondMostPrefferedChannel.Value);
                ThirdMostPrefferedChannel = Convert.ToString(p_ThirdMostPrefferedChannel.Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Return
            return lst;
        }
    }

    public class usp_GetDashboardResult
    {
        public int? TotalGroups { get; set; }
        public int? TotalMembers { get; set; }
        public int? TotalNotices { get; set; }
    }

    public class usp_GetGroupMappingUsersResult
    {
        public Guid UserID { get; set; }
        public string UserName { get; set; }
        public string EmailID { get; set; }
        public string MobileNo { get; set; }
        public string FacebookID { get; set; }
        public string DeviceToken { get; set; }
        public string ConnectionID { get; set; }
        public string SlackID { get; set; }
    }

    public class usp_GetNoticesListResult
    {
        public int NoticeID { get; set; }
        public Guid UserID { get; set; }
        public string UserName { get; set; }
        public string NoticeTitle { get; set; }
        public string NoticeDetail { get; set; }
        public string NoticeDate { get; set; }
        public string LastReplyDate { get; set; }
        public string FileName { get; set; }
        public int FileType { get; set; }
        public int IsReply { get; set; }
        public string LatestMessage { get; set; }
        public string FilePath { get; set; }
        public bool? IsRead { get; set; }
    }

    public class usp_GetNoticesList_RepliesResult
    {
        public int NoticeID { get; set; }
        public Guid UserID { get; set; }
        public string UserName { get; set; }
        public string NoticeTitle { get; set; }
        public string NoticeDetail { get; set; }
        public string NoticeDate { get; set; }
        public string FileName { get; set; }
        public int FileType { get; set; }
        public int IsReply { get; set; }
        public string FilePath { get; set; }
        public bool? IsRead { get; set; }
        public string MobileNo { get; set; }
    }

    public class usp_GetUserGroupsResult
    {
        public int GroupID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? TotalMembers { get; set; }
        public string LastRead { get; set; }
        public int? ReplyReadPendingCount { get; set; }
        public bool? IsPrivate { get; set; }
    }

    public class usp_GetUserLoginResult
    {
        public Guid UserID { get; set; }
        public string UserName { get; set; }
        public string EmailID { get; set; }
        public int ClientID { get; set; }
        public string MobileNo { get; set; }
        public bool? IsAdmin { get; set; }
        public string ClientName { get; set; }
        public string ProfilePic { get; set; }
        public string ClientLogo { get; set; }
        public string ClientEmail { get; set; }
        public bool? IsPasswordUpdated { get; set; }
        public bool IsIntroHappened { get; set; }
    }

    public class usp_InserMsgWAResult
    {
        public int? NoticeID { get; set; }
        public int? GroupID { get; set; }
        public string UserId { get; set; }
    }

    public class usp_User_SelectByGroupIDResult
    {
        public Guid UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailID { get; set; }
        public bool? IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public string CreateDate { get; set; }
        public string MobileNo { get; set; }
        public string ProfilePhoto { get; set; }
        public int IsAdmin { get; set; }
    }

    public class usp_User_SelectByListIDResult
    {
        public Guid UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailID { get; set; }
        public bool? IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public string CreateDate { get; set; }
        public string MobileNo { get; set; }
        public string ProfilePhoto { get; set; }
    }

    public class usp_GetUserByNoticeReadResult
    {
        public int NoticeID { get; set; }
        public bool? IsRead { get; set; }
        public string FirstName { get; set; }
        public string MobileNo { get; set; }
    }

    public class usp_InsertReceiveEmailResult
    {
        public int? NoticeID { get; set; }
        public int? GroupID { get; set; }
        public string UserID { get; set; }
    }

    public class usp_InsertReceiveSlackResult
    {
        public int? NoticeID { get; set; }
        public int? GroupID { get; set; }
        public string UserID { get; set; }
        public bool IsAlreadyExists { get; set; }
    }

    public class usp_GetScheduledNoticeListResult
    {
        public string Subject { get; set; }
        public string Description { get; set; }
        public DateTime? StartTime { get; set; }
        public string Location { get; set; }
        public string StartTimezone { get; set; }
        public DateTime? EndTime { get; set; }
        public string EndTimezone { get; set; }
        public int IsAllDay { get; set; }
        public string RecurrenceRule { get; set; }
    }

    public class usp_Report_GetCampaignReportDetailsResult
    {
        public int NoticeID { get; set; }
        public string Message { get; set; }
        public string Sender { get; set; }
        public string NoticeDate { get; set; }
        public string FirstChannel { get; set; }
        public string SecondChannel { get; set; }
        public string ThirdChannel { get; set; }
        public string FirstPercentage { get; set; }
        public string SecondPercentage { get; set; }
        public string ThirdPercentage { get; set; }
    }

    public class usp_Report_GetCampaignReportHeadingResult
    {
        public string Channel { get; set; }
    }

    public class usp_Report_GetGroupReportDetailsResult
    {
        public int GroupID { get; set; }
        public string Name { get; set; }
        public int MembersCount { get; set; }
        public string CreatedDate { get; set; }
        public string LastMsgDate { get; set; }
        public int? MsgCount { get; set; }
        public string ActivityLevel { get; set; }
    }

    public class usp_Report_GetGroupReportHeadingResult
    {
        public int? TotalGroups { get; set; }
        public int? TotalMembers { get; set; }
        public int? TotalNotices { get; set; }
    }

    public class usp_Report_GetReadReportDetailsResult
    {
        public string Member { get; set; }
        public string MobileNo { get; set; }
        public string EchoDate { get; set; }
        public string EchoStatus { get; set; }
        public string SMSDate { get; set; }
        public string SMSStatus { get; set; }
        public string EmailDate { get; set; }
        public string EmailStatus { get; set; }
        public string WhatsAppDate { get; set; }
        public string WhatsAppStatus { get; set; }
    }

    public class usp_GetListByClientIDResult
    {
        public int ListID { get; set; }
        public string Name { get; set; }
        public int? TotalMembers { get; set; }
        public string CreatedByUser { get; set; }
        public string CreatedDate { get; set; }
    }

    public class usp_Report_ChannelVerificationResult
    {
        public Guid UserID { get; set; }
        public string UserName { get; set; }
        public string ProfilePhoto { get; set; }
        public bool IsOnSMS { get; set; }
        public bool IsOnEmail { get; set; }
        public bool IsOnWhatsApp { get; set; }
        public bool IsOnTelegram { get; set; }
    }

    public class usp_Report_GetCampaignSummaryReportResult
    {
        public int Campaigns { get; set; }
        public int Audience { get; set; }
        public int Groups { get; set; }
        public int MessagesSent { get; set; }
        public int Responses { get; set; }
        public int Engagement { get; set; }
        public int Seen { get; set; }
        public int OptOut { get; set; }
    }

    public class usp_Report_GetChannelInsightReportResult
    {
        public string ChannelName { get; set; }
        public int Audience { get; set; }
        public int Sent { get; set; }
        public int Delivered { get; set; }
        public int Seen { get; set; }
        public int Responded { get; set; }
        public int OptOut { get; set; }
    }

    public class usp_Report_GetWeekdaysEngagementReportResult
    {
        public string ChannelName { get; set; }
        public int Sent { get; set; }
        public int Weekend { get; set; }
        public int Weekday { get; set; }
    }
    public class usp_Report_GetChannelSavingReportResult
    {
        public string ChannelName { get; set; }
        public int? Valid { get; set; }
        public int ArtificialIntelligence { get; set; }
        public int? Invalid { get; set; }
    }

    public class usp_Report_GetChannelTrendsReportResult
    {
        public string ChannelName { get; set; }
        public int Q1 { get; set; }
        public int Q2 { get; set; }
        public int Q3 { get; set; }
        public int Q4 { get; set; }
    }

    public class usp_GetUsersForPendingNoticeResult
    {
        public Guid UserID { get; set; }
        public string UserName { get; set; }
        public string ConnectionID { get; set; }
        public string EmailID { get; set; }
        public string MobileNo { get; set; }
        public string FacebookID { get; set; }
        public string DeviceToken { get; set; }
    }

    public class usp_GetSchedularDataResult
    {
        public int ID { get; set; }
        public string NoticeTitle { get; set; }
        public string NoticeDetailHTML { get; set; }
        public bool IsSms { get; set; }
        public bool IsEmail { get; set; }
        public bool IsWhatsapp { get; set; }
        public bool IsFacebook { get; set; }
        public bool IsSlack { get; set; }
        public bool IsInstagram { get; set; }
        public bool IsViber { get; set; }
        public bool IsLine { get; set; }
        public bool IsWeChat { get; set; }
        public bool IsPushNotification { get; set; }
        public bool IsBroadcastToAllChannel { get; set; }
        public int GroupId { get; set; }
        public int ParentId { get; set; }
        public DateTime ScheduleTime { get; set; }
        public DateTime Timestamp { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int FileType { get; set; }
        public string FileName { get; set; }
        public string TemplateId { get; set; }
    }


    public class usp_GetTemplateDataResult
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Body { get; set; }
        public string Type { get; set; }
        public bool IsSms { get; set; }
        public bool IsEmail { get; set; }
        public bool IsWhatsapp { get; set; }
        public bool IsFacebook { get; set; }
        public bool IsSlack { get; set; }
        public bool IsInstagram { get; set; }
        public bool IsViber { get; set; }
        public bool IsLine { get; set; }
        public bool IsWeChat { get; set; }
        public bool IsPushNotification { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class usp_GetUserIdsFromGroupResult
    {
        public Guid UserId { get; set; }
    }

    public class usp_GetNoticeDetailsForHubResult
    {
        public int NoticeID { get; set; }
        public string NoticeDetail { get; set; }
        public DateTime NoticeDate { get; set; }
        public int GroupID { get; set; }
        public int ParentID { get; set; }
        public Guid UserID { get; set; }
        public string MobileNo { get; set; }
        public string UserName { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
    }

    public class usp_ChannelPredictionFileResult
    {
        public int NoticeID { get; set; }
        public string NoticeDetail { get; set; }
        public int Size_MB { get; set; }
        public int Age { get; set; }
        public string Location { get; set; }
        public string UserID { get; set; }
        public int Delivered_Success { get; set; }
        public int Open_Success { get; set; }
        public string Time_Delivered { get; set; }
        public string Weekday_weekend { get; set; }
        public string Time_open { get; set; }
        public int Timespan_Btw_del_open { get; set; }
        public string Segment_type { get; set; }
        public string Channel_name { get; set; }
        public int Responded { get; set; }
        public string ResponseTime { get; set; }
        public int IsText { get; set; }
        public int IsImage { get; set; }
        public int IsVideo { get; set; }
        public int IsDocument { get; set; }
    }

    public class usp_GetChannelPredictionResult
    {
        public string Member { get; set; }
        public DateTime JoiningDate { get; set; }
        public string PreferredChannel { get; set; }
    }
    public class usp_Report_GetDNDReportDetailsResult
    {
        public Guid userID { get; set; }
        public string Member { get; set; }
        public string GroupName { get; set; }
        public string CreatedDate { get; set; }
        public string ConductedBy { get; set; }
        public string DNDStatus { get; set; }
    }

}