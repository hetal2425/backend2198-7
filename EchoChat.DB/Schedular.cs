﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EchoChat.DB
{
    public partial class Schedular
    {
        public int ID { get; set; }
        public string NoticeTitle { get; set; }
        public string NoticeDetailHTML { get; set; }
        public bool IsSms { get; set; }
        public bool IsEmail { get; set; }
        public bool IsWhatsapp { get; set; }
        public bool IsFacebook { get; set; }
        public bool IsSlack { get; set; }
        public bool IsInstagram { get; set; }
        public bool IsViber { get; set; }
        public bool IsLine { get; set; }
        public bool IsWeChat { get; set; }
        public bool IsPushNotification { get; set; }
        public bool IsBroadcastToAllChannel { get; set; }
        public int GroupId { get; set; }
        public int ParentId { get; set; }
        public DateTime ScheduleTime { get; set; }
        public DateTime Timestamp { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FileName { get; set; }
        public int FileType { get; set; }
        public string TemplateId { get; set; }
    }
}
