﻿using EchoChat.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EchoChat.Repository
{
    public interface INoticeRepository
    {
        bool SaveNoticeOutput(int noticeID, int clientID, ConcurrentBag<ChannelOutput> output);
        void SeenNotice(int noticeID, Guid userID, string type);
        NoticeDetailDTO GetNoticeDetailsByNoticeID(int noticeID);
        NoticeSmallDataModel GetNoticeListByGroupID(int parentID, int pageNo, int pageSize, Guid userID, string freeText, int timeZoneOffset);
        NoticeSmallDataModel GetNoticeRepliesListByGroupID(int parentID, int pageNo, int pageSize, Guid userID, string freeText, int timeZoneOffset);
        List<DeletedNotice> DeleteNotice(int noticeID);
        bool SaveNoticeDetails(NoticeDetailDTO noticeDetails);
        bool SaveNotice(List<UserChannelInfo> userChannelInfos, int noticeID, int groupID, Guid createdBy);
        List<ScheduledNotices> GetScheduledNotices(DateTime fromDate, DateTime toDate, int timeZoneOffset);
        bool SaveIntegrationNoticeDetails(NoticeDetailDTO noticeDetails);
        bool SaveIntegrationNotice(List<UserChannelInfo> userChannelInfos, int noticeID, int groupID, Guid createdBy);
        bool SavePrivateNoticeDetails(NoticeDetailDTO noticeDetails);
        bool SavePrivateNotice(List<UserChannelInfo> userChannelInfos, int noticeID, int groupID, Guid createdBy);
        List<string> GetConfigWhatsAppTemplate();
        List<string> GetConfigEmailTemplate();
        HubNotice GetNoticeDetailsForHub(int noticeID);
    }
}
