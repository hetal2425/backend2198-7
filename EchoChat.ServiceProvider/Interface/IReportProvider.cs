﻿using EchoChat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace EchoChat.ServiceProvider
{
    public interface IReportProvider
    {
        GroupReportHeading GetGroupReportHeading(int clientID);
        GroupReportDetailsModel GetGroupReportDetails(int clientID, string freeText, DateTime? fromDate, DateTime? toDate, int pageSize, int pageNo, int timeZoneOffset);
        List<CampaignReportHeading> GetCampaignReportHeading(int groupID);
        CampaignReportDetailsModel GetCampaignReportDetails(int groupID, string freeText, DateTime? fromDate, DateTime? toDate, int pageSize, int pageNo, int timeZoneOffset);
        ReadReportDetailsModel GetReadReportDetails(int noticeID, string freeText, int pageSize, int pageNo, int timeZoneOffset);
        ChannelVerificationModel GetChannelVerification(int clientID, int pageSize, int pageNo, string freeText);
        public bool BlockNotice(int noticeID);
        List<ChannelSavingModel> GetChannelSavingReportDetails(int clientID, DateTime? fromDate, DateTime? toDate, int timeZoneOffset);
        List<ChannelInsightModel> GetChannelInsightReportDetails(int clientID, int groupID, int noticeID, DateTime? fromDate, DateTime? toDate, int timeSlot, int timeZoneOffset);
        List<WeekdaysEngagementModel> GetWeekdaysEngagementReportDetails(int clientID, DateTime? fromDate, DateTime? toDate, int dayType, int timeSlot, int timeZoneOffset);
        List<CampaignSummaryModel> GetCampaignSummaryReportDetails(int clientID, int groupID, int noticeID, DateTime? fromDate, DateTime? toDate, int timeZoneOffset);
        List<ChannelProgressModel> GetChannelProgressReportDetails(int clientID, int timeZoneOffset);
        void AddUpdateChannelPrediction(ChannelPredictionModel channelPrediction);
        List<ChannelPredictionFileModel> GenerateChannelPredictionFile();
        ListOfChannelPrediction GetChannelPrediction(string slot, string search, int pageSize, int pageNo);
    }
}
