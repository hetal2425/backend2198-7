﻿using EchoChat.Models;
using EchoChat.Repository;
using System;
using System.Collections.Generic;

namespace EchoChat.ServiceProvider
{
    public class ReportProvider : IReportProvider
    {
        IReportRepository reportRepository;
        public ReportProvider(IReportRepository oReportRepository)
        {
            reportRepository = oReportRepository;
        }

        public GroupReportHeading GetGroupReportHeading(int clientID)
        {
            try
            {
                return reportRepository.GetGroupReportHeading(clientID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public GroupReportDetailsModel GetGroupReportDetails(int clientID, string freeText, DateTime? fromDate, DateTime? toDate, int pageSize, int pageNo, int timeZoneOffset)
        {
            try
            {
                return reportRepository.GetGroupReportDetails(clientID, freeText, fromDate, toDate, pageSize, pageNo, timeZoneOffset);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CampaignReportHeading> GetCampaignReportHeading(int groupID)
        {
            try
            {
                return reportRepository.GetCampaignReportHeading(groupID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CampaignReportDetailsModel GetCampaignReportDetails(int groupID, string freeText, DateTime? fromDate, DateTime? toDate, int pageSize, int pageNo, int timeZoneOffset)
        {
            try
            {
                return reportRepository.GetCampaignReportDetails(groupID, freeText, fromDate, toDate, pageSize, pageNo, timeZoneOffset);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ReadReportDetailsModel GetReadReportDetails(int noticeID, string freeText, int pageSize, int pageNo, int timeZoneOffset)
        {
            try
            {
                return reportRepository.GetReadReportDetails(noticeID, freeText, pageSize, pageNo, timeZoneOffset);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ChannelVerificationModel GetChannelVerification(int clientID, int pageSize, int pageNo, string freeText)
        {
            try
            {
                return reportRepository.GetChannelVerification(clientID, pageSize, pageNo, freeText);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool BlockNotice(int noticeID)
        {
            try
            {
                return reportRepository.BlockNotice(noticeID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ChannelSavingModel> GetChannelSavingReportDetails(int clientID, DateTime? fromDate, DateTime? toDate, int timeZoneOffset)
        {
            try
            {
                return reportRepository.GetChannelSavingReportDetails(clientID, fromDate, toDate, timeZoneOffset);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ChannelInsightModel> GetChannelInsightReportDetails(int clientID, int groupID, int noticeID, DateTime? fromDate, DateTime? toDate, int timeSlot, int timeZoneOffset)
        {
            try
            {
                return reportRepository.GetChannelInsightReportDetails(clientID, groupID, noticeID, fromDate, toDate, timeSlot, timeZoneOffset);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<WeekdaysEngagementModel> GetWeekdaysEngagementReportDetails(int clientID, DateTime? fromDate, DateTime? toDate, int dayType, int timeSlot, int timeZoneOffset)
        {
            try
            {
                return reportRepository.GetWeekdaysEngagementReportDetails(clientID, fromDate, toDate, dayType, timeSlot, timeZoneOffset);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<CampaignSummaryModel> GetCampaignSummaryReportDetails(int clientID, int groupID, int noticeID, DateTime? fromDate, DateTime? toDate, int timeZoneOffset)
        {
            try
            {
                return reportRepository.GetCampaignSummaryReportDetails(clientID, groupID, noticeID, fromDate, toDate, timeZoneOffset);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<ChannelProgressModel> GetChannelProgressReportDetails(int clientID, int timeZoneOffset)
        {
            try
            {
                return reportRepository.GetChannelProgressReportDetails(clientID, timeZoneOffset);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void AddUpdateChannelPrediction(ChannelPredictionModel channelPrediction)
        {
            try
            {
                reportRepository.AddUpdateChannelPrediction(channelPrediction);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ChannelPredictionFileModel> GenerateChannelPredictionFile()
        {
            try
            {
                return reportRepository.GenerateChannelPredictionFile();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ListOfChannelPrediction GetChannelPrediction(string slot, string search, int pageSize, int pageNo)
        {
            try
            {
                return reportRepository.GetChannelPrediction(slot, search, pageSize, pageNo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}