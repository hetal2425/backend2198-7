﻿using EchoChat.DB;
using EchoChat.Models;
using EchoChat.Repository;
using System;
using System.Collections.Generic;

namespace EchoChat.ServiceProvider
{
    public class UserProvider : IUserProvider
    {
        IUserRepository userRepository;
        public UserProvider(IUserRepository oUserRepository)
        {
            userRepository = oUserRepository;
        }

        public void AddUserConnection(string connectionID, Guid userID)
        {
            try
            {
                userRepository.AddUserConnection(connectionID, userID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool CreateUpdateUser(CreateUser user, int clientID, Guid createdBy)
        {
            try
            {
                return userRepository.CreateUpdateUser(user, clientID, createdBy);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteUser(Guid userID)
        {
            try
            {
                return userRepository.DeleteUser(userID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool FirstTimeUpdatePassword(UpdatePassword update)
        {
            try
            {
                return userRepository.FirstTimeUpdatePassword(update);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public UserProfile GetUserByID(Guid userID)
        {
            try
            {
                return userRepository.GetUserByID(userID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public UserProfile GetUserBySlackID(string slackID)
        {
            try
            {
                return userRepository.GetUserBySlackID(slackID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CreateUser GetUserByUsername(string username)
        {
            try
            {
                return userRepository.GetUserByUsername(username);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public UserClaimsModel GetUserClaimsByID(Guid userID)
        {
            try
            {
                return userRepository.GetUserClaimsByID(userID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<UserSmallModel> GetUsersByListIDs(List<int> listIDs)
        {
            try
            {
                return userRepository.GetUsersByListIDs(listIDs);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveUserConnection(string connectionID)
        {
            try
            {
                userRepository.RemoveUserConnection(connectionID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SetGroupAdmin(GroupAdminMapping mapping)
        {
            try
            {
                return userRepository.SetGroupAdmin(mapping);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int SubmitUsers(ExcelUsers list, Guid userID, int clientID)
        {
            try
            {
                return userRepository.SubmitUsers(list, userID, clientID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdatePassword(UpdatePassword update)
        {
            try
            {
                return userRepository.UpdatePassword(update);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateIntroHappened(Guid userID)
        {
            try
            {
                return userRepository.UpdateIntroHappened(userID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool UpdateUserPersonalInfo(UserPersonalInfoModel user, Guid userID)
        {
            try
            {
                return userRepository.UpdateUserPersonalInfo(user, userID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetUserPasswordByID(Guid userID)
        {
            try
            {
                return userRepository.GetUserPasswordByID(userID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool UpdateUserPassword(UserPasswordModel user, Guid userID)
        {
            try
            {
                return userRepository.UpdateUserPassword(user, userID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetUserConnection(Guid userID)
        {
            try
            {
                return userRepository.GetUserConnection(userID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool SaveUpdateSetting(AppSettingModel appSetting)
        {
            try
            {
                return userRepository.SaveUpdateSetting(appSetting);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool DeleteSetting(int appSettingId)
        {
            try
            {
                return userRepository.DeleteSetting(appSettingId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<AppSettingModel> GetAppSettingByChannel(String channel)
        {
            try
            {
                return userRepository.GetAppSettingByChannel(channel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<string> GetSettingKeys(bool isKarix)
        {
            try
            {
                return userRepository.GetSettingKeys(isKarix);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CreateUser CreateIntegrationUser(CreateUser user, int clientID, Guid createdBy)
        {
            try
            {
                return userRepository.CreateIntegrationUser(user, clientID, createdBy);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<UserListDTO> GetUserIdsFromGroup(int groupId)
        {
            try
            {
                return userRepository.GetUserIdsFromGroup(groupId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool SubmitWhatsAppParameters(List<ExcelWhatsAppData> list, Guid userID, int clientID)
        {
            try
            {
                return userRepository.SubmitWhatsAppParameters(list, userID, clientID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<WhatsAppParameterData> GetWhatsAppParameters(string Template)
        {
            try
            {
                return userRepository.GetWhatsAppParameters(Template);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
