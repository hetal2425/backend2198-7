﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EchoChat.Models
{
    public class AppVersionDTO
    {
        public string Platform { get; set; }
        public string Version { get; set; }
        public string Date { get; set; }
    }
}
