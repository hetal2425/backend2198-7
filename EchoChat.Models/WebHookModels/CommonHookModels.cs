﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace EchoChat.Models.WebHookModels
{
    public class NoticeVM
    {
        public string NoticeTitle { get; set; }
        public string NoticeDetail { get; set; }
        public string Createdby { get; set; }
        public string NoticeDate { get; set; }
        public string GroupID { get; set; }
        public string IsReply { get; set; }
        public string IsSms { get; set; }
        public string ParentId { get; set; }
        public string FileType { get; set; }
        public string FileName { get; set; }
        public IFormFile StreamWithData { get; set; }
        public string IsEmail { get; set; }
        public string IsWhatsapp { get; set; }

    }
}
